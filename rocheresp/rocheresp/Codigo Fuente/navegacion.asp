<%

abrirBaseDatos()
var Msql="select Actividades.*,actividades_categorias.categoria,actividades_categorias.columna  from Actividades inner join actividades_categorias on actividades_categorias.idcategoria=Actividades.idcategoria where IdActividad>0 and actividades.activo='1' and actividades_categorias.activa='1' "
Msql+=" order by actividades_categorias.columna,actividades_categorias.orden,Actividades.orden "
var MiRecordset =  Server.CreateObject("ADODB.Recordset");
MiRecordset.Open(Msql,Conexion,3,1);
amenu = new Array();
while(!MiRecordset.EOF)
{
        if(typeof(amenu[String(MiRecordset('columna'))])=='undefined')
        {
            amenu[String(MiRecordset('columna'))]= new Array();
        }
        if(typeof(amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))])=='undefined')
        {
            amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))] = new Array();
            amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['categoria']=String(MiRecordset('categoria'))
            amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades']=new Array();
        }
        
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]  =new Array();
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]['titulocorto']=String(MiRecordset('titulocorto'));
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]['actividad']=String(MiRecordset('actividad'));
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]['logistica']=String(MiRecordset('logistica'));
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]['responsable']=String(MiRecordset('responsable'));
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]['ejemplos']=String(MiRecordset('ejemplos'));
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]['tipoevento']=String(MiRecordset('tipoevento'));
        amenu[String(MiRecordset('columna'))][String(MiRecordset('idcategoria'))]['actividades'][String(MiRecordset('idactividad'))]['aspectos']=String(MiRecordset('aspectos'));
    MiRecordset.MoveNext();
}

MiRecordset.Close();

activodef="";
activopla="";
activoact="";

if(menuactivo=='actividades')                                    
    activoact=" active "
if(menuactivo=='definiciones')                                    
    activodef=" class='active' "
if(menuactivo=='planificaciones')                                    
    activopla=" class='active' "
 
%>
<nav class="nav-main mega-menu">
	<ul class="nav nav-pills nav-main" id="mainMenu">
		<li <%=activodef%>>
			<a href="<%=Application("web")%>index.asp">
				Definiciones
			</a>
		</li>
		<li <%=activopla%>>
			<a href="<%=Application("web")%>eventos-planificados.asp">
				Planificación
			</a>
		</li>
		<li class="dropdown mega-menu-item mega-menu-fullwidth <%=activoact%>"  >
			<a class="dropdown-toggle" href="<%=Application("web")%>actividades.asp">
				Actividades
				<i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu">
				<li>
					<div class="mega-menu-content">
						<div class="row">
						
						
						<%
						for(columna in amenu )
						{
						%>
						    <div class="col-md-4">
								<ul class="sub-menu">
									<li>
						            <%
						            for(categoria in amenu[columna] )
						            {
						            %>
						                <span class="mega-menu-sub-title"> <%=amenu[columna][categoria]['categoria']%></span>
										<ul class="sub-menu">
									        <%
					                        for(actividad in amenu[columna][categoria]['actividades'] )
					                        {
					                            strurl=Application("web")+"actividad.asp?idactividad="+actividad;
					                            if(Session("rewrite"))
					                            {
					                                
                                                    
                                                    titulocorto =removeDiacritics(amenu[columna][categoria]['actividades'][actividad]['titulocorto'])
										                titulocorto = urlify(titulocorto)
					                                strurl=Application("web")+"actividades/"+actividad+"/"+titulocorto;
					                             }
					                                
					                        %>	
											<li><a href="<%=strurl%>"><%=amenu[columna][categoria]['actividades'][actividad]['titulocorto']%></a></li>
											<%
						                    }
						                    %>
										</ul> 
						            <%
						            }
						            %>
						      			
									</li>
								</ul>
							</div>
						<%
						}
						%>
						
							
						</div>
					</div>
				</li>
			</ul>
		</li>
		
	</ul>
</nav>
<%
cerrarBaseDatos()
%>
