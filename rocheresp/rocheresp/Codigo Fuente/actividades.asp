<%@ Language=JScript codepage="65001" %>
<!--#include file ="controlnet/includes/variables.asp"-->
<!--#include file ="controlnet/includes/funciones.asp"-->
<!--#include file ="controlnet/includes/funcionesbasedatos.asp"-->
<!DOCTYPE html>
<%
menuactivo='actividades'
abrirBaseDatos()
select="select *  from definiciones where activo='1' "
var MiRecordset =  Server.CreateObject("ADODB.Recordset");
MiRecordset.Open(select,Conexion,3,1);
adefiniciones = new Array();
atitular = new Array();
atitular[0] = new Array();
atitular[0]['titulo'] = '';
atitular[0]['definicion'] = '';

while(!MiRecordset.EOF)
{
    if (String(MiRecordset('tipo')) != 'titular') {
        adefiniciones[String(MiRecordset('iddefinicion'))] = new Array();
        adefiniciones[String(MiRecordset('iddefinicion'))]['titulo'] = String(MiRecordset('titulo'));
        adefiniciones[String(MiRecordset('iddefinicion'))]['definicion'] = String(MiRecordset('definicion'));
    }
    else {
        atitular[0]['titulo'] = String(MiRecordset('titulo'));
        atitular[0]['definicion'] = String(MiRecordset('definicion'));
    }
    MiRecordset.MoveNext();
}

MiRecordset.Close();
                                    
%>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Roche</title>		
		<meta name="description" content="">
		<meta name="author" content="f2f digital">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">


		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

	</head>
	<body>
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="index.asp">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="img/logo.png">
						</a>
					</div>
					
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						
						<!--#include file ="navegacion.asp"-->
					</div>
				</div>
			</header>

			<div role="main" class="main">

				<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li><a href="index.asp">Inicio</a></li>
									<li class="active">Actividades</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

                    
                    <div class="row">
						<div class="col-md-12">
							<h2><strong>Actividades</strong></h2>


							<div class="row">
							
							
							<%
						for(columna in amenu )
						{
						%>
						    <div class="col-sm-4">
								
						            <%
						            for(categoria in amenu[columna] )
						            {
						            %>
						                <h4><%=amenu[columna][categoria]['categoria']%></h4>
						                
									        <%
					                        for(actividad in amenu[columna][categoria]['actividades'] )
					                        {
					                            iconfirm='fa-group'
					                            if(amenu[columna][categoria]['actividades'][actividad]['responsable']=='Médico')
					                                iconfirm='fa-stethoscope'
					                            
					                        %>	
											<div class="feature-box">
											    <div class="feature-box-icon">
											        <i class="fa <%=iconfirm%>"></i>
										        </div>
										        <div class="feature-box-info">
										        <%
										            strurl = Application("web") + "actividad.asp?idactividad=" + actividad;
										            if (Session("rewrite")) {

										                //titulocorto = createSEOTitle(amenu[columna][categoria]['actividades'][actividad]['titulocorto'])
										                titulocorto =removeDiacritics(amenu[columna][categoria]['actividades'][actividad]['titulocorto'])
										                titulocorto = urlify(titulocorto)
										                strurl = Application("web") + "actividades/" + actividad + "/" + titulocorto;
										            }
										        %>
											        <h5 class="shorter"><a href="<%=strurl%>"><%=amenu[columna][categoria]['actividades'][actividad]['titulocorto']%></a></h5>
                                                    <p class="tall"><i>Responsable:</i> <%=amenu[columna][categoria]['actividades'][actividad]['responsable']%> <br/>
											        <i>Logística:</i> <%=amenu[columna][categoria]['actividades'][actividad]['logistica']%></p>

										        </div>
										    </div>
											<%
						                    }
						                    %>
										
						            <%
						            }
						            %>
						      			
									
							</div>
						<%
						}
						%>
						
							</div>
						</div>

					</div>
					<div class="row leyenda">
							<strong>Leyenda:</strong><br>
							<div class="description-leyend"><div class="feature-box-icon"><i class="fa fa-stethoscope"></i></div>Responsable médico</div>
							<div class="description-leyend"><div class="feature-box-icon"><i class="fa fa-group"></i></div>Responsable comercial
							</div>
                    </div>            
                   
				</div>

			</div>

			<footer id="footer">
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<p>© Copyright 2015. All Rights Reserved.</p>
							</div>
							
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.js"></script>
  

		<script src="vendor/jquery.easing/jquery.easing.js"></script>
		<script src="vendor/bootstrap/bootstrap.js"></script>
		<script src="vendor/common/common.js"></script>		

		<!-- Theme Base, Components and Settings 	-->
		<script src="js/theme.js"></script>
	
		<!-- Theme Custom         	-->
		<script src="js/custom.js"></script>

		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>


	</body>
</html>
