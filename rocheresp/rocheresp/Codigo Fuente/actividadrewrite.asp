<%@ Language=JScript codepage="65001" %>
<!--#include file ="controlnet/includes/variables.asp"-->
<!--#include file ="controlnet/includes/funciones.asp"-->
<!--#include file ="controlnet/includes/funcionesbasedatos.asp"-->
<!DOCTYPE html>
<%
    idactividad = Request2('idactividad');
    title = Request2('titulo');
    
    
        
    
if(idactividad!=parseInt(idactividad,10))
    Response.Redirect(Application("web")+"page-404.asp");



menuactivo = 'actividades'
abrirBaseDatos()
select="select *  from actividades where idactividad='"+idactividad+"' and activo='1' "
var MiRecordset =  Server.CreateObject("ADODB.Recordset");
MiRecordset.Open(select,Conexion,3,1);

aActividades = new Array();
if (MiRecordset.EOF)
    Response.Redirect(Application("web") + "page-404.asp");
else
{

    titulocorto = removeDiacritics(String(MiRecordset('titulocorto')))
    titulocorto = urlify(titulocorto)

    if (title != titulocorto) {
        Response.Redirect(Application("web") + "page-404.asp");
    }
    
    
    
    aActividades[String(MiRecordset('idactividad'))] = new Array();
    aActividades[String(MiRecordset('idactividad'))]['actividad'] = String(MiRecordset('Actividad'));

    aActividades[String(MiRecordset('idactividad'))]['titulocorto'] = String(MiRecordset('titulocorto'));
    aActividades[String(MiRecordset('idactividad'))]['logistica'] = String(MiRecordset('logistica'));
    aActividades[String(MiRecordset('idactividad'))]['responsable'] = String(MiRecordset('responsable'));
    aActividades[String(MiRecordset('idactividad'))]['ejemplos'] = String(MiRecordset('ejemplos'));
    aActividades[String(MiRecordset('idactividad'))]['tipoevento'] = String(MiRecordset('tipoevento'));
    aActividades[String(MiRecordset('idactividad'))]['aspectos'] = String(MiRecordset('aspectos'));
    
    aActividades[String(MiRecordset('idactividad'))]['pasos'] = new Array();
    select = "select *  from Actividadespasos where activo=1 and idactividad='" + String(MiRecordset('idactividad')) + "' order by orden,substring(paso,0,100) "
    var MiRecordset1 = Server.CreateObject("ADODB.Recordset");
    MiRecordset1.Open(select, Conexion, 3, 1);
    while (!MiRecordset1.EOF) {

        aActividades[String(MiRecordset('idactividad'))]['pasos'][String(MiRecordset1('idpaso'))] = new Array();
        aActividades[String(MiRecordset('idactividad'))]['pasos'][String(MiRecordset1('idpaso'))]['paso'] = String(MiRecordset1('paso'));
        aActividades[String(MiRecordset('idactividad'))]['pasos'][String(MiRecordset1('idpaso'))]['que'] = String(MiRecordset1('que'));
        aActividades[String(MiRecordset('idactividad'))]['pasos'][String(MiRecordset1('idpaso'))]['quien'] = String(MiRecordset1('quien'));
        aActividades[String(MiRecordset('idactividad'))]['pasos'][String(MiRecordset1('idpaso'))]['donde'] = String(MiRecordset1('donde'));
        aActividades[String(MiRecordset('idactividad'))]['pasos'][String(MiRecordset1('idpaso'))]['cuando'] = String(MiRecordset1('cuando'));

        MiRecordset1.MoveNext();
    }
    MiRecordset1.Close();
    
    
}

MiRecordset.Close();


                                    
%>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Roche</title>		
		<meta name="description" content="">
		<meta name="author" content="f2f digital">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<%=Application("web")%>vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="<%=Application("web")%>vendor/fontawesome/css/font-awesome.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<%=Application("web")%>css/theme.css">
		<link rel="stylesheet" href="<%=Application("web")%>css/theme-elements.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<%=Application("web")%>css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<%=Application("web")%>css/custom.css">


		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

	</head>
	<body>
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="<%=Application("web")%>index.asp">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="<%=Application("web")%>img/logo.png">
						</a>
					</div>
					
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
					
						<!--#include file ="navegacion.asp"-->	
					
					</div>
				</div>
			</header>

			<div role="main" class="main">

				<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li><a href="<%=Application("web")%>index.asp">Inicio</a></li>
									<li><a href="<%=Application("web")%>actividades.asp">Actividades</a></li>
									<li class="active"><%=aActividades[idactividad]['titulocorto']%></li>
								</ul>
							</div>
						</div>

					</div>
				</section>

				<div class="container">

					<div class="row">

						<div class="col-md-12">

							<h2 class="shorter"><strong><%=aActividades[idactividad]['actividad']%></strong></h2>
                            <div class="col-md-4">
                            <%
                            if(aActividades[idactividad]['tipoevento']!='')
                            {
                            %>
                            	<h4>Tipo de evento</h4>
    							<%=aActividades[idactividad]['tipoevento']%>
                            <%}
                            %>    
                            <%
                            if(aActividades[idactividad]['ejemplos']!='')
                            {
                            %>
                            	<h4>Ejemplos:</h4>
    							<%=aActividades[idactividad]['ejemplos']%>
                            <%}
                            %>    
                                
    							
                                
                            </div>

                            <div class="col-md-8">
                             <%
                            if(aActividades[idactividad]['aspectos']!='')
                            {
                            %>
                                <h4>Aspectos a tener en cuenta</h4>
    
    							<%=aActividades[idactividad]['aspectos']%>
    						<%}
                            %>    
                            </div>
						</div>
					</div>
					<hr class="tall" />
				</div>

				<div class="container">

					<div class="row">

						<div class="col-md-12">
							<h3 class="short"><strong>Pasos</strong></h3>
							<p>Pulsa sobre cada uno de los pasos para ver más información.</p>
                            
                            
                            <div class="toggle" data-plugin-toggle data-plugin-options='{ "isAccordion": true }'>
                            
                             <%
                             
                                pasoold='';
                                ipos=0;
					            for(paso in aActividades[idactividad]['pasos'] )
					            {
					                if(pasoold!=aActividades[idactividad]['pasos'][paso]['paso'])
					                {
					                     if(ipos>0)
					                     {
					                      %>
    					                  </table>
									        </div>
								            </section>
					                      <%  
					                     }   
					                    %>
					                    <section class="toggle"> <!--active-->
								        <label><%=aActividades[idactividad]['pasos'][paso]['paso']%></label>
								        <div class="toggle-content">
								        <table>
									        <tr>
										        <th><strong>QUÉ</strong></th>
										        <th><strong>QUIÉN</strong></th>
										        <th><strong>DÓNDE</strong></th>
										        <th><strong>DOCUMENTACIÓN</strong></th>
									        </tr>
								    <%} %>
									        <tr>
										        <td width="25%">
										        <p><%=aActividades[idactividad]['pasos'][paso]['que']%></p>
										        </td>
										        <td width="25%">
										        <p><%=aActividades[idactividad]['pasos'][paso]['quien']%></p>
										        </td>
										        <td width="25%">
										        <p><%=aActividades[idactividad]['pasos'][paso]['donde']%></p>
										        </td>
										        <td>
										        <p><%=aActividades[idactividad]['pasos'][paso]['cuando']%></p>
										        </td>
									        </tr>
									        <%
									      
                                    pasoold=aActividades[idactividad]['pasos'][paso]['paso']
                                    ipos++;
                                }
                                if(ipos>0)
                                {
                                %>		
                                </table>
									</div>
								</section>
								<%} %>
                            
							</div>
						</div>

					</div>

				</div>

			</div>

			<footer id="footer">
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<p>© Copyright 2015. All Rights Reserved.</p>
							</div>
							
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="<%=Application("web")%>vendor/jquery/jquery.js"></script>
  

		<script src="<%=Application("web")%>vendor/jquery.easing/jquery.easing.js"></script>
		<script src="<%=Application("web")%>vendor/bootstrap/bootstrap.js"></script>
		<script src="<%=Application("web")%>vendor/common/common.js"></script>		

		<!-- Theme Base, Components and Settings 	-->
		<script src="<%=Application("web")%>js/theme.js"></script>
	
		<!-- Theme Custom         	-->
		<script src="<%=Application("web")%>js/custom.js"></script>

		
		<!-- Theme Initialization Files -->
		<script src="<%=Application("web")%>js/theme.init.js"></script>


	</body>
</html>
