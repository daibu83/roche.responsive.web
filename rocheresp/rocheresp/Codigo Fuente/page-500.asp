<%@ Language=JScript codepage="65001" %>
<!--#include file ="controlnet/includes/variables.asp"-->
<!--#include file ="controlnet/includes/funciones.asp"-->
<!--#include file ="controlnet/includes/funcionesbasedatos.asp"-->
<!DOCTYPE html>
<%
menuactivo = ''
abrirBaseDatos()
                                    
%>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Roche</title>		
		<meta name="description" content="">
		<meta name="author" content="f2f digital">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">


		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

	</head>
	<body>
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="index.asp">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="img/logo.png">
						</a>
					</div>
					
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						
						<!--#include file ="navegacion.asp"-->
					</div>
				</div>
			</header>

            <div role="main" class="main">

				<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li class="active">Se ha producido un error</li>
								</ul>
							</div>
						</div>
						
					</div>
				</section>
				

			<div role="main" class="main">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h1>Se ha producido un error</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<section class="page-not-found">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
								<div class="page-not-found-main">
									<h2> <i class="fa fa-file"></i></h2>
									<p>Se ha producido un error</p>
								</div>
							</div>
							<div class="col-md-4">
								<h4 class="heading-primary">¿Dónde quieres ir?</h4>
								<ul class="nav nav-list">
									<li><a href="<%=Application("web")%>index.asp">Definiciones</a></li>
									<li><a href="<%=Application("web")%>eventos-planificados.asp">Planificación</a></li>
									<li><a href="<%=Application("web")%>actividades.asp">Actividades</a></li>
								</ul>
							</div>
						</div>
					</section>

				</div>

			</div>
			

			<footer id="footer">
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<p>© Copyright 2015. All Rights Reserved.</p>
							</div>
							
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.js"></script>
  

		<script src="vendor/jquery.easing/jquery.easing.js"></script>
		<script src="vendor/bootstrap/bootstrap.js"></script>
		<script src="vendor/common/common.js"></script>		

		<!-- Theme Base, Components and Settings 	-->
		<script src="js/theme.js"></script>
	
		<!-- Theme Custom         	-->
		<script src="js/custom.js"></script>

		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>


	</body>
</html>
<%
cerrarBaseDatos()
%>