﻿
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" > 
	<head>
		<title>Roche. Administración.</title>
		<meta name="description"		content="" />
		<meta name="keywords"  			content="" />
		<meta name="copyright" 			content="" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<script type="text/javascript" src="js/swfobject.js"></script>
		<script type="text/javascript" src="js/funciones.js"></script>
		
		 <!-- Jquery directly from google servers--> 
		  <script type="text/javascript" src="js/jquery.tools.min.js"></script>
		    <script type="text/javascript" src="js/jquery.fixedheader.js"></script>
                     
   
		 <!-- Javascript for client side table sort--> 
		<script type="text/javascript" src="js/tinytable.js"></script>
		
		<!-- Javascript for client side table sort--> 
		<script src="js/jquery.tablesorter.js" type="text/javascript"></script>
		<script src="js/jquery.tablesorter.pager.js" type="text/javascript"></script>
		<!-- datePicker -->
		<script src="js/es.js" type="text/javascript"></script>
		<script src="js/datepicker.packed.js" type="text/javascript"></script>
        <script src="js/tinymce/tinymce.min.js" type="text/javascript"></script>
		
		 <!-- WYSIWYG Editor --> 
		<script type="text/javascript" src="js/jquery.wysiwyg.js"></script> 
		
		 <!-- Style switcher --> 
		
		<script type="text/javascript" src="js/stylesheetToggle.js"></script>
        <link rel="stylesheet" type="text/css" href="css/overlay.css">
       
        
		<link rel="stylesheet" type="text/css" media="all" href="css/reset.css" />
		<link rel="stylesheet" type="text/css" media="all" href="css/green.css" />
		<link rel="stylesheet" type="text/css" media="all" href="css/admin.css" />
        <link rel="stylesheet" type="text/css" media="all" href="css/tablesorter.css" />
		<link rel="stylesheet" type="text/css" href="css/datepicker.css">
		
		<!-- comment extra.css for css validation -->
		<link rel="stylesheet" type="text/css" media="all" href="css/extra.css" />
		
		<!-- See Interface Configuration --> 
		<script type="text/javascript" src="js/seeui.js"></script>
		
		<!--[if IE 6]>
		    <link rel="stylesheet" type="text/css" href="css/overlaye6.css">
			<script type="text/javascript" src="js/ddbelatedpng.js"></script>
			<script type="text/javascript">	
				DD_belatedPNG.fix('img, .info a');
			</script>
		<![endif]-->

		<!--   <script type="text/javascript"></script>    -->
		<script type="text/javascript">
		$(function(){
			
		//ACORDEON
		$("#accordion").tabs("#accordion ul", {tabs: 'h3', effect: 'slide' });
		
		});
		
		function DescargarExcell(Tabla,NombreTabla)
        {
            var datos=document.getElementById(Tabla).innerHTML
            
             document.getElementById("ContenidoExcell").value=datos
            document.getElementById("NombreExcell").value=NombreTabla
            
            
            document.getElementById("fVerExcel").submit()
            
      
        }
        function ExportarGrafico(strGrafico)
        {    
            flashGrafico = document.getElementById(strGrafico);
            flashGrafico.exportImage("exportar/export.aspx"); 
        
        }
</script>
	</head>
		
	<body>
	<form name="fVerExcel" id="fVerExcel" method="post"   target="VerExcel" action="excell/verexcell.asp" style="width:0px; height:0px;" enctype="multipart/form-data">
<input type=hidden name="ContenidoExcell" id="ContenidoExcell">
<input type=hidden name="NombreExcell" id="NombreExcell">
<iframe name="VerExcel" id="VerExcel" width=0 height=0 frameborder=0></iframe>
</form>
	