﻿<%@ Language=JScript codepage="65001" %>
<!--#include file ="includes/seguridad.asp"-->
<!--#include file ="includes/variables.asp"-->
<!--#include file ="inicio.asp"-->
<!--#include file ="includes/funciones.asp"-->
<!--#include file ="includes/funcionesbasedatos.asp"-->
<!--#include file ="includes/buscadores.asp"-->
<%
abrirBaseDatos()
var IdDefinicion=Request2("IdDefinicion")

var strTextoBusqueda=""

if(Definicion !='')
    strTextoBusqueda+="Definicion: "+Definicion+". "    

bOperar = false;
bOperar = GetOperar();    
%>
		<div id="bk">
		<!--#include file ="header.asp"-->
		<!--#include file ="tooltip.asp"-->
		<div id="container" class="clearfix">
			  <!--#include file ="menu.asp"-->
			<div id="page">
				<div class="menu clearfix">
					 <!-- PAGE TITLE --> 
					<div>Definiciones</div>
					<div class="cr_pass"></div>
				</div><!-- menu -->
				
				 <!-- PAGE CONTENT --> 
				<div class="clearfix content">
					 <!-- Tab bar component --> 
					<div class="clearfix tabbar Definiciones">
						<ul class="tabs">
							<li class="tab1" id="tab1">
							<%
							
							var Ocultartab3=' style="display:none" '
							var Ocultartab4=' style="display:none" '
							var Seleccionartab1=' class="selected" '
							var Seleccionartab4=' class="selected" '
							if(IdDefinicion!='')
							{
							    Ocultartab4=""
							    Seleccionartab1=""
							}
							else
							{
							    Ocultartab3=""
							    Seleccionartab4=""
							}
							
							%>
								<a id="a1" <%=Seleccionartab1%> onclick="showOnly('tab1','Definiciones')" >
									Listado
								</a>
							</li>
							<li class="tab2" id="tab2">
								<a id="a2" onclick="showOnly('tab2','Definiciones')">
									<img src="img/icons/magnifier.png" alt=""/>
									Buscador
								</a>
							</li>
							<!--
							<li class="tab3" <%=Ocultartab3%> id="tab3">
								<a id="a3" onclick="showOnly('tab3','Definiciones')">
									<img src="img/icons/plus-small.png" alt=""/>
									Nuevo Definiciones
								</a>
							</li>
							-->
							<li class="tab4" <%=Ocultartab4%> id="tab4">
								<a id="a4" <%=Seleccionartab4%> onclick="showOnly('tab4','Definiciones')">
									<img src="img/icons/edit.png" alt=""/>
									Ver editar
								</a>
							</li>
						</ul>
						<div class="tabContent clearfix">
						<!-- tab 1 content -->
							<div class="tab1">
								<%
								if(strTextoBusqueda!='')
								{
								%>
									<div class="box-padd">
									<p class="text filtrored">Atención, tienes filtros de búsqueda aplicados. Resultados de la búsqueda. <%=strTextoBusqueda%><strong></strong></p>
								</div><!-- box-padd -->
								<%
						        }
						        %>
								<table cellpadding="0" cellspacing="0" border="0" id="table"  class="uiTable tablesorter">  
								
									<thead> 
										<tr> 
											<th><h3>Definicion</h3></th> 
											<th><h3>Tipo</h3></th> 
												<th><h3>Activo</h3></th> 
											<th><h3>Editar</h3></th>
											<!--
											<th><h3>Eliminar</h3></th> 											
											-->
										</tr> 
									</thead> 
									<tbody> 
									
										<%
									var Msql="select * from  definiciones where Iddefinicion>0 "
									if(Definicion!='')
									    Msql+=" and definicion like '%"+Definicion+"%' or titulo like '%"+Definicion+"%' "
									
									Msql+=" order by titulo"
									
									//Response.Write(Msql)
                                    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    								MiRecordset.Open(Msql,Conexion,3,1);
                                    var iNumUsuario=0
                                     if(MiRecordset.EOF)
                                    {
                                        Response.Write('<tr><td colspan="2">No hay datos para el criterio de busqueda</td></tr>')
                                    }
                                    var strDefinicion=""
                                    var strIdDefinicion=""
                                    var strInicial=""
                                    var strFinal=""
                                    var strOrden=""
                                    var strTipo=""
                                        
                                    while(!MiRecordset.EOF)
                                    {
                                        strDefinicion=""+MiRecordset("Definicion")
                                        strTitulo=""+MiRecordset("Titulo")
                                        strIdDefinicion=""+MiRecordset("IdDefinicion")
                                        strActivo=""+MiRecordset("Activo")
                                        strTipo=""+MiRecordset("Tipo")
                                        strIconoActivo="minus-circle.png"
                                        if(strActivo==1)
                                            strIconoActivo="tick.png"
                                        
                                      
                                        
									%>
									
										<tr> 
											<td ><%=strTitulo%></td> 
											<td ><%=strTipo%></td> 
											
											<td><img src="img/icons/<%=strIconoActivo%>" class="icon" /></td> 
											<td style="width:200px"><a href="javascript:EditarTipo('<%=strIdDefinicion%>')" title="Editar"><img src="img/icons/edit.png" alt="Editar"/></a></td>  
											
										</tr> 
									<%
									    MiRecordset.MoveNext()
									    iNumUsuario++
									}
									MiRecordset.Close()
									%>
																		
									</tbody> 
								</table>
								 <!-- Navigation controls --> 
								<br class="clear" />
								<div class="barbottom">
									<!-- pagination -->
									<div id="pager" class="pager">
										<form action="#">
										<fieldset>
											<div class="fright">
												<a class="butNav first" href="#">&laquo; First</a>
												<a class="butNav prev" href="#">&laquo; Previous</a>
												<a class="butPage" href="#"><input type="text" class="pagedisplay"/></a>
												<a class="butNav next" href="#">Next &raquo;</a>
												<a class="butNav last" href="#">Last &raquo;</a>
											</div>
											<label for="sizefiltro" >Mostrar datos:</label>
											<select id="sizefiltro" class="pagesize" >
											    
												
												<%
												if(iNumUsuario>=10)
												    Response.Write('<option value="10">10</option>')
												if(iNumUsuario>=20)
												    Response.Write('<option value="20">20</option>')
												if(iNumUsuario>=30)
												    Response.Write('<option value="30">30</option>')    
												if(iNumUsuario>=40)
												    Response.Write('<option value="40">40</option>')    
												if(iNumUsuario!=10 && iNumUsuario!=20 && iNumUsuario!=30 && iNumUsuario!=40)    
												{
												%>
    												<option  value="<%=iNumUsuario%>"><%=iNumUsuario%></option>
    										    <%
    										    }
    										    %>
											</select>
											</fieldset>
										</form>
									</div><!-- pager -->
								</div><!-- barbottom -->
							</div><!-- tab1 -->
							
							<div class="tab2" style="display:none;">
    							<form name="Form1" id="Form1" method=post>
                                <input type=hidden name="IdDefinicion" id="IdDefinicion">    
                                <input type=hidden name="TipoBusqueda" id="TipoBusqueda" value="S">    
								<!--  Tab 2 content -->
								<div class="fields clearfix">
									<h2>Rellena los datos del formulario para buscar</h2>
									<p class="sep">
										<label class="medium" for="nombre">Definicion</label>
										<input type="text" value="<%=Definicion%>" class="sText" id="Definicion" name="Definicion"/>
									</p>
								
								</div><!-- fields -->
								<div class="boxBut"><input type="submit" value="Buscar" class="butDef" /></div>
								<div class="boxButfiltro"><input  id="clearfilter" value="Eliminar Filtros" class="butDef" /></div>
								<!-- boxBut -->
	                            </form>							
							</div><!-- tab2 -->
							
							<div class="tab3" style="display:none;">
								<!--  Tab 3 content -->
									
							</div><!-- tab3-->
							<div class="tab4" style="display:none;">
								
								<form name="Form2" id="Form2" method=post target="frEjecutar" action="Definicionesguardar.asp">
									<div class="fields clearfix">
									<% 
										
										
										var strUnicoIdDefinicion=""
										var strUnicoDefinicion=""
										var strUnicoTitulo = ""
                                        var strUnicoActivo=""
                                        var strTipo="";
                                        if(IdDefinicion!='')
                                        {
                                            var strSelect=" select * from Definiciones where IdDefinicion='"+IdDefinicion+"' "
                                            
                                            MiRecordset.Open(strSelect,Conexion,3,1);
                                            
                                            if(!MiRecordset.EOF) {
                                                strTipo = "" + MiRecordset("Tipo")
                                                strUnicoIdDefinicion=""+MiRecordset("IdDefinicion")
                                                strUnicoDefinicion=""+MiRecordset("Definicion")
                                                strUnicoActivo = "" + MiRecordset("Activo")
                                                strUnicoTitulo = "" + MiRecordset("Titulo")
                                                
                                                
								            }
								            MiRecordset.Close()
								      	
								        }
								        
								        //Response.Write("//"+strUnicoTipo)
										%>
										<input type=hidden name="strUnicoIdDefinicion" id="strUnicoIdDefinicion" value="<%=strUnicoIdDefinicion%>"/>
										<h2>Modifica los datos del formulario para editar 
										<a href="javascript:CerrarEditar()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep clearfix">
											<label class="fleft medium" for="imgAdd1">Posición</label>
											<%=strTipo%>
											
										</p>
										<p class="sep clearfix">
											<label class="fleft medium" for="imgAdd1">Título</label>
											<input type="text"  class="sText"  id="strUnicoTitulo" name="strUnicoTitulo" value="<%=strUnicoTitulo%>"/>
											
										</p>
				
										<div class="sep">
											<label class="medium" for="nombreAdd1">Definicion</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="15" id="strUnicoDefinicion" name="strUnicoDefinicion"><%=strUnicoDefinicion%></textarea>
											</div>
										</div>
										
										
									</div><!-- fields -->
									<div class="fields">
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivo" name="strUnicoActivo">
												<%
												    DibujarActivadosBinario(strUnicoActivo)
												%>
											</select>
										</p>
									</div><!-- fields -->
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarEditar()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<%
										if(bOperar==1)
										{
										%>
										    <input type=button value="Guardar" class="butDef" onclick="javascript:EnviarEditar()"/>
										<%
										}
										%>
									</div><!-- fright-->
									</form>
							</div><!-- tab4-->
						</div><!-- tabContent-->
					</div><!-- tabbar-->
					<!--  PAGE CONTENT  -->
				</div><!-- content -->
			</div><!-- page -->
		</div><!-- container -->
		</div><!-- bk -->
		
		
		 
<iframe id="frEjecutar" name="frEjecutar" width="1" height="0" frameborder="0" ></iframe>

	</body>
</html>
<script type="text/javascript">
IdDefinicion='<%=IdDefinicion%>'


if(IdDefinicion!='')
showOnly('tab4','Definiciones')

$(function(){
    $("#table") 
    .tablesorter({widthFixed: false, widgets: ['zebra']}) 
    .tablesorterPager({container: $("#pager")});   

});

$(function(){
    $('#clearfilter').click(function() {
        document.getElementById('frEjecutar').src="includes/limpiarfiltros.asp"
    });
});
    


function EditarTipo(IdDefinicion)
{
    document.getElementById('IdDefinicion').value=IdDefinicion
    document.getElementById('Form1').submit()
}
function CerrarEditar()
{
    document.getElementById('tab4').style.display="none"
    //document.getElementById('tab3').style.display="block"
    showOnly('tab1','Definiciones')
    $(".tabbar .tabs a").removeClass('selected');
	$("#a1").addClass("selected");
     
}
function EnviarEditar()
{
    
    
    
    if (Trim(document.getElementById('strUnicoDefinicion').value) == null || Trim(document.getElementById('strUnicoDefinicion').value) == "")
    {
        alert ("Campo Definicion obligatorio");
        document.getElementById('strUnicoDefinicion').focus()
        return;
    }

    if (Trim(document.getElementById('strUnicoTitulo').value) == null || Trim(document.getElementById('strUnicoTitulo').value) == "")
    {
        alert ("Campo Título obligatorio");
        document.getElementById('strUnicoTitulo').focus()
        return;
    }
    
   
    
    if (!Confirmar("¿Seguro que quiere modificar estos datos?"))
    return

    document.getElementById('Form2').submit()
}
function CerrarNuevo()
{
    showOnly('tab1','Definiciones')
    
   	$(".tabbar .tabs a").removeClass('selected');
    $("#a1").addClass("selected");
 
}
function ControlarNumero(objeto)
{
    var pattern = /[^0-9\.]/g; // cualquier cosa que no sea numero y punto;
    objeto.value = objeto.value.replace(pattern, '');
}
tinymce.init({
    mode: "textareas",

    theme_advanced_buttons1: "link, unlink,",
    force_br_newlines: true,
    force_p_newlines: false,
    forced_root_block: false,
    plugins: "paste,link,code",
    cleanup: false,
    verify_html: false


});
</script>
<%
cerrarBaseDatos()
%>
