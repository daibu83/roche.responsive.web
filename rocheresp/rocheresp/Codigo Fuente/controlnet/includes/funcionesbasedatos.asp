﻿
<%

function abrirBaseDatos(){
// FUNCIONALIDAD: Abre la base de datos, establece de fecha (dia/mes/año) y devuelve una conexión.   
	
	try{Conexion.Open(Session("Conexion"));}catch(e){}
	try{Conexion.Execute("SET DATEFORMAT ymd");}catch(e){}
}

function cerrarBaseDatos(){
// FUNCIONALIDAD: Cierra la base de datos.
	try{Conexion.Close();}catch(e){}
	try{Conexion=null;}catch(e){}
}
function DibujarActivadosBinario(Activo)
{
    Response.Write('<option value="">Selecciona una opción</option>')
    
    strSelected=""
    if(Activo=='1')
        strSelected=" Selected "
    
    Response.Write('<option '+strSelected+' value="1">Sí</option>')
    
    strSelected=""
    if(Activo=='0')
        strSelected=" Selected "
    Response.Write('<option '+strSelected+' value="0">No</option>')
}
function GetUltimoLogueo(strIdUsuario)
{
    var Msql="select top 1 convert(nvarchar,fecha,21) as fecha from Logueos where IdUsuario='"+strIdUsuario+"' order by fecha desc "
    var Temas1 =  Server.CreateObject("ADODB.Recordset");
	Temas1.Open(Msql, Conexion,3,1);
    strReturn=""
    
    if(!Temas1.EOF)
        strReturn=''+Temas1("fecha")
    
    Temas1.Close()
    
    return strReturn
}
                                                
function DibujarRol(Tipo)
{
    Response.Write('<option value="">Selecciona un rol</option>')
    
    strSelected=""
    if(Tipo=='G')
        strSelected=" Selected "
    
    Response.Write('<option '+strSelected+' value="G">Administrar contenidos</option>')
    strSelected=""
    if(Tipo=='A')
        strSelected=" Selected "
    
    Response.Write('<option '+strSelected+' value="A">Administrador</option>')
    
}
function DibujarActivados(Activo)
{
    Response.Write('<option value="">Selecciona una opción</option>')
    
    strSelected=""
    if(Activo=='S')
        strSelected=" Selected "
    
    Response.Write('<option '+strSelected+' value="S">Sí</option>')
    
    strSelected=""
    if(Activo=='N')
        strSelected=" Selected "
    Response.Write('<option '+strSelected+' value="N">No</option>')
}
function DibujarOrden(orden)
{
    Response.Write('<option value="">Selecciona una opción</option>')
    for(iPos=1;iPos<=200;iPos++)
    {
        strSelected=""
        if(orden==iPos)
            strSelected=" Selected "
        Response.Write('<option '+strSelected+' value="'+iPos+'">'+iPos+'</option>')
    }
}
function DibujarColumnas(columna)
{
    Response.Write('<option value="">Selecciona una opción</option>')
    for(iPos=1;iPos<=3;iPos++)
    {
        strSelected=""
        if(columna==iPos)
            strSelected=" Selected "
        Response.Write('<option '+strSelected+' value="'+iPos+'">'+iPos+'</option>')
    }
}
function DibujarPlanificaciones(IdPlanificacion)
{
      Response.Write('<option value="">Selecciona una planificación</option>')
    
    	 var Msql="select * from Planificaciones  "
        
    Msql+=" order by orden "
    
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(Msql,Conexion,3,1);
    var strZona=""
    var strIdZona=""
    while(!MiRecordset.EOF)
    {
        strZona=""+MiRecordset("Planificacion")
        strIdZona=""+MiRecordset("idplanificacion")
        strSelected=""
        if(strIdZona==IdPlanificacion)
            strSelected=" Selected "
        Response.Write('<option value="'+strIdZona+'" '+strSelected+' >'+strZona+'</option>')
        MiRecordset.MoveNext()
    }
    
    MiRecordset.Close()
}
function GetPlanificacion(IdP)
{
    var Msql="select * from Planificaciones where IdPlanificacion='"+IdP+"'  "
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(Msql,Conexion,3,1);
    var strPlanificacion=""
    if(!MiRecordset.EOF)
    {
        strPlanificacion=""+MiRecordset("Planificacion")
    }
    
    MiRecordset.Close()
    
    return strPlanificacion
}


function DibujarActividades(IdActividad)
{
      Response.Write('<option value="">Selecciona una actividad</option>')
    
    	 var Msql="select * from Actividades  "
        
    Msql+=" order by orden "
    
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(Msql,Conexion,3,1);
    var strZona=""
    var strIdZona=""
    while(!MiRecordset.EOF)
    {
        strZona=""+MiRecordset("Actividad")
        strIdZona=""+MiRecordset("IdActividad")
        strSelected=""
        if(strIdZona==IdActividad)
            strSelected=" Selected "
        Response.Write('<option value="'+strIdZona+'" '+strSelected+' >'+strZona+'</option>')
        MiRecordset.MoveNext()
    }
    
    MiRecordset.Close()
}
function GetActividad(IdP)
{
    var Msql="select * from Actividades where IdActividad='"+IdP+"'  "
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(Msql,Conexion,3,1);
    var strPlanificacion=""
    if(!MiRecordset.EOF)
    {
        strPlanificacion=""+MiRecordset("Actividad")
    }
    
    MiRecordset.Close()
    
    return strPlanificacion
}

function DibujarCategorias(IdCategoria)
{
      Response.Write('<option value="">Selecciona una categoría</option>')
    
    	 var Msql="select * from actividades_categorias  "
        
    Msql+=" order by columna,orden "
    
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(Msql,Conexion,3,1);
    var strZona=""
    var strIdZona=""
    while(!MiRecordset.EOF)
    {
        strZona=""+MiRecordset("categoria")
        strIdZona=""+MiRecordset("idcategoria")
        strSelected=""
        if(strIdZona==IdCategoria)
            strSelected=" Selected "
        Response.Write('<option value="'+strIdZona+'" '+strSelected+' >'+strZona+'</option>')
        MiRecordset.MoveNext()
    }
    
    MiRecordset.Close()
}
function GetCategoria(IdP)
{
    var Msql="select * from actividades_categorias where idcategoria='"+IdP+"'  "
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(Msql,Conexion,3,1);
    var strPlanificacion=""
    if(!MiRecordset.EOF)
    {
        strPlanificacion=""+MiRecordset("categoria")
    }
    
    MiRecordset.Close()
    
    return strPlanificacion
}
function DibujarResponsables(Res)
{
    Response.Write('<option value="">Selecciona una opción</option>')
    
    strSelected=""
    if(Res=='Comercial')
        strSelected=" Selected "
    
    Response.Write('<option '+strSelected+' value="Comercial">Comercial</option>')
    
    strSelected=""
    if(Res=='Médico')
        strSelected=" Selected "
    Response.Write('<option '+strSelected+' value="Médico">Médico</option>')
    
    strSelected=""
    if(Res=='Comercial y/o Médico')
        strSelected=" Selected "
    Response.Write('<option '+strSelected+' value="Comercial y/o Médico">Comercial y/o Médico</option>')
}

function ordenarcategorias(strUnicoIdCategoria,strUnicoColumnaN,strUnicoOrdenN,strUnicoActivoN)
{
    aCategorias= new Array()
    iorden=1;
    myquery="select *  from actividades_categorias where IdCategoria!='"+strUnicoIdCategoria+"' and columna="+strUnicoColumnaN +" order by activa desc,columna,orden"
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(myquery,Conexion,3,1);
    var binsert=false;
    while(!MiRecordset.EOF)
    {
        if((iorden==strUnicoOrdenN && strUnicoActivoN==1 && !binsert ) || (strUnicoActivoN==0 && String(MiRecordset("activa"))=='0'  && iorden>=strUnicoOrdenN && !binsert) )
        {
        
            aCategorias[iorden]=strUnicoIdCategoria
            iorden++;
            binsert=true;
        }
        aCategorias[iorden]=""+MiRecordset("IdCategoria")
        MiRecordset.MoveNext();
        iorden++;
        
    }
    MiRecordset.Close();
    
    if(!binsert)
    {
        aCategorias[iorden]=strUnicoIdCategoria
    }
    
    for(key in aCategorias)
    {
        strupdate="update actividades_categorias set orden="+key+" where idcategoria="+aCategorias[key];
        Conexion.Execute(strupdate)
    }
    
    
}

function ordenaractividades(strUnicoIdActividad,strUnicoIdCategoria,strUnicoOrdenN,strUnicoActivoN)
{
    aActividades= new Array()
    iorden=1;
    myquery="select *  from actividades where IdActividad!='"+strUnicoIdActividad+"' and idcategoria="+strUnicoIdCategoria +" order by activo desc,idcategoria,orden"
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(myquery,Conexion,3,1);
    var binsert=false;
    while(!MiRecordset.EOF)
    {
        if((iorden==strUnicoOrdenN && strUnicoActivoN==1 && !binsert ) || (strUnicoActivoN==0 && String(MiRecordset("activo"))=='0'  && iorden>=strUnicoOrdenN && !binsert) )
        {
            aActividades[iorden]=strUnicoIdActividad
            iorden++;
            binsert=true;
        }
        aActividades[iorden]=""+MiRecordset("idactividad")
        MiRecordset.MoveNext();
        iorden++;
    }
    MiRecordset.Close();
    if(!binsert)
        aActividades[iorden]=strUnicoIdActividad
    
    for(key in aActividades)
    {
        strupdate="update actividades set orden="+key+" where idactividad="+aActividades[key];
        Conexion.Execute(strupdate)
    }
}

function ordenaractividadespasos(strUnicoIdPaso,strUnicoIdActividadN,strUnicoOrdenN,strUnicoActivoN)
{
    aActividades= new Array()
    iorden=1;
    myquery="select *  from Actividadespasos where IdPaso!='"+strUnicoIdPaso+"' and idactividad="+strUnicoIdActividadN +" order by activo desc,idactividad,orden"
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(myquery,Conexion,3,1);
    var binsert=false;
    while(!MiRecordset.EOF)
    {
        if((iorden==strUnicoOrdenN && strUnicoActivoN==1 && !binsert ) || (strUnicoActivoN==0 && String(MiRecordset("activo"))=='0'  && iorden>=strUnicoOrdenN && !binsert) )
        {
            aActividades[iorden]=strUnicoIdPaso
            iorden++;
            binsert=true;
        }
        aActividades[iorden]=""+MiRecordset("idpaso")
        MiRecordset.MoveNext();
        iorden++;
    }
    MiRecordset.Close();
    if(!binsert)
        aActividades[iorden]=strUnicoIdPaso
    
    for(key in aActividades)
    {
        strupdate="update Actividadespasos set orden="+key+" where idpaso="+aActividades[key];
        Conexion.Execute(strupdate)
    }
}


function ordenarolanificaciones(strUnicoIdPlanificacion,strUnicoOrdenN,strUnicoActivoN)
{
    aPlanificaciones= new Array()
    iorden=1;
    myquery="select *  from Planificaciones where IdPlanificacion!='"+strUnicoIdPlanificacion+"' order by activo desc,orden"
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(myquery,Conexion,3,1);
    var binsert=false;
    while(!MiRecordset.EOF)
    {
        if((iorden==strUnicoOrdenN && strUnicoActivoN==1 && !binsert ) || (strUnicoActivoN==0 && String(MiRecordset("activo"))=='0'  && iorden>=strUnicoOrdenN && !binsert) )
        {
            aPlanificaciones[iorden]=strUnicoIdPlanificacion
            iorden++;
            binsert=true;
        }
        aPlanificaciones[iorden]=""+MiRecordset("idplanificacion")
        MiRecordset.MoveNext();
        iorden++;
    }
    MiRecordset.Close();
    if(!binsert)
        aPlanificaciones[iorden]=strUnicoIdPlanificacion
    
    for(key in aPlanificaciones)
    {
        strupdate="update Planificaciones set orden="+key+" where IdPlanificacion="+aPlanificaciones[key];
        Conexion.Execute(strupdate)
    }
}

function ordenarplanificacionespasos(strUnicoIdPaso,strUnicoIdPlanificacionN,strUnicoOrdenN,strUnicoActivoN)
{
    aPlanificaciones= new Array()
    iorden=1;
    myquery="select *  from Planificacionespasos where IdPaso!='"+strUnicoIdPaso+"' and IdPlanificacion="+strUnicoIdPlanificacionN +" order by activo desc,IdPlanificacion,orden"
    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(myquery,Conexion,3,1);
    var binsert=false;
    while(!MiRecordset.EOF)
    {
        if((iorden==strUnicoOrdenN && strUnicoActivoN==1 && !binsert ) || (strUnicoActivoN==0 && String(MiRecordset("activo"))=='0'  && iorden>=strUnicoOrdenN && !binsert) )
        {
            aPlanificaciones[iorden]=strUnicoIdPaso
            iorden++;
            binsert=true;
        }
        aPlanificaciones[iorden]=""+MiRecordset("idpaso")
        MiRecordset.MoveNext();
        iorden++;
    }
    MiRecordset.Close();
    if(!binsert)
        aPlanificaciones[iorden]=strUnicoIdPaso
    
    for(key in aPlanificaciones)
    {
        strupdate="update Planificacionespasos set orden="+key+" where idpaso="+aPlanificaciones[key];
        Conexion.Execute(strupdate)
    }
}
%>

