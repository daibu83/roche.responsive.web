﻿<%@ Language=JScript codepage="65001" %>
<!--#include file ="includes/seguridad.asp"-->
<!--#include file ="includes/variables.asp"-->
<!--#include file ="inicio.asp"-->
<!--#include file ="includes/funciones.asp"-->
<!--#include file ="includes/funcionesbasedatos.asp"-->
<!--#include file ="includes/buscadores.asp"-->
<%
abrirBaseDatos()
var IdPlanificacion=Request2("IdPlanificacion")


var strTextoBusqueda=""

if(Planificacion !='')
    strTextoBusqueda+="Planificacion : "+Planificacion +". "

bOperar = false;
bOperar = GetOperar();
    
%>
		<div id="bk">
		<!--#include file ="header.asp"-->
		<!--#include file ="tooltip.asp"-->
		<div id="container" class="clearfix">
			  <!--#include file ="menu.asp"-->
			<div id="page">
				<div class="menu clearfix">
					 <!-- PAGE TITLE --> 
					<div>Planificaciones Encintados</div>
					<div class="cr_pass"></div>
				</div><!-- menu -->
				
				 <!-- PAGE CONTENT --> 
				<div class="clearfix content">
					 <!-- Tab bar component --> 
					<div class="clearfix tabbar Planificaciones">
						<ul class="tabs">
							<li class="tab1" id="tab1">
							<%
							
							var Ocultartab3=' style="display:none" '
							var Ocultartab4=' style="display:none" '
							var Seleccionartab1=' class="selected" '
							var Seleccionartab4=' class="selected" '
							if(IdPlanificacion!='')
							{
							    Ocultartab4=""
							    Seleccionartab1=""
							}
							else
							{
							    Ocultartab3=""
							    Seleccionartab4=""
							}
							
							%>
								<a id="a1" <%=Seleccionartab1%> onclick="showOnly('tab1','Planificaciones')" >
									Listado
								</a>
							</li>
							<li class="tab2" id="tab2">
								<a id="a2" onclick="showOnly('tab2','Planificaciones')">
									<img src="img/icons/magnifier.png" alt=""/>
									Buscador
								</a>
							</li>
							<li class="tab3" <%=Ocultartab3%> id="tab3">
								<a id="a3" onclick="showOnly('tab3','Planificaciones')">
									<img src="img/icons/plus-small.png" alt=""/>
									Planificacion Nueva
								</a>
							</li>
							<li class="tab4" <%=Ocultartab4%> id="tab4">
								<a id="a4" <%=Seleccionartab4%> onclick="showOnly('tab4','Planificaciones')">
									<img src="img/icons/edit.png" alt=""/>
									Ver editar
								</a>
							</li>
						</ul>
						<div class="tabContent clearfix">
						<!-- tab 1 content -->
							<div class="tab1">
								<%
								if(strTextoBusqueda!='')
								{
								%>
									<div class="box-padd">
									<p class="text filtrored">Atención, tienes filtros de búsqueda aplicados. Resultados de la búsqueda. <%=strTextoBusqueda%><strong></strong></p>
								</div><!-- box-padd -->
								<%
						        }
						        %>
								<table cellpadding="0" cellspacing="0" border="0" id="table"  class="uiTable tablesorter">  
								
									<thead> 
										<tr>
											<th><h3>Planificacion</h3></th> 
											<th><h3>Orden</h3></th> 
											<th><h3>Pasos</h3></th> 
											<th><h3>Activo</h3></th> 
											<th><h3>Editar</h3></th>
											<!--
											<th><h3>Eliminar</h3></th> 											
											-->
										</tr> 
									</thead> 
									<tbody> 
									
										<%
									var Msql="select *  from Planificaciones where IdPlanificacion>0  "
									if(Planificacion!='')
									    Msql+=" and Planificacion like '%"+Planificacion+"%' "
									
									Msql+=" order by orden, Planificacion "
                                    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    								MiRecordset.Open(Msql,Conexion,3,1);
                                    var iNumPlanificaciones=0
                                     if(MiRecordset.EOF)
                                    {
                                        Response.Write('<tr><td colspan="4">No hay datos para el criterio de busqueda</td></tr>')
                                    }
                                    while(!MiRecordset.EOF)
                                    {
                                        strPlanificacion=""+MiRecordset("Planificacion")
                                        strIdPlanificacion=""+MiRecordset("IdPlanificacion")
                                        strActivo=""+MiRecordset("Activo")
                                        strOrden=""+MiRecordset("Orden")
                                        strIcono="minus-circle.png"
                                        if(strActivo==1)
                                            strIcono="tick.png"
                                        select="select paso,idpaso from planificacionespasos where idplanificacion="+strIdPlanificacion +" order by orden "
                                        strPasos="";
                                         var MiRecordset1 =  Server.CreateObject("ADODB.Recordset");
    								    MiRecordset1.Open(select,Conexion,3,1);
    								    while(!MiRecordset1.EOF)
                                        {
                                            
                                            strPasos+="<a style='text-decoration:underline'  href='planificacionespasos.asp?TipoBusqueda=S&IdPaso="+String(MiRecordset1('idpaso'))+"&IdPlanificacionbuscar="+strIdPlanificacion+"' >"+String(MiRecordset1('paso'))+"</a></br>"
                                            MiRecordset1.MoveNext();
                                        }
                                        MiRecordset1.Close();
                                        
                                        
                                            
                                        
                                        
									%>
									
										<tr> 
											<td style="width:200px"><%=strPlanificacion%></td> 
											<td style="width:200px"><%=strOrden%></td> 
											<td style="width:200px"><%=strPasos%></td> 
											
											<td><img src="img/icons/<%=strIcono%>" class="icon" alt="Planificaciones"/></td> 

											<td><a href="javascript:EditarPlanificacion('<%=strIdPlanificacion%>')" title="Editar"><img src="img/icons/edit.png" alt="Editar"/></a></td>  
											
										</tr> 
									<%
									    MiRecordset.MoveNext()
									    iNumPlanificaciones++
									}
									MiRecordset.Close()
									%>
																		
									</tbody> 
								</table>
								 <!-- Navigation controls --> 
								<br class="clear" />
								<div class="barbottom">
									<!-- pagination -->
									<div id="pager" class="pager">
										<form action="#">
										<fieldset>
											<div class="fright">
												<a class="butNav first" href="#">&laquo; First</a>
												<a class="butNav prev" href="#">&laquo; Previous</a>
												<a class="butPage" href="#"><input type="text" class="pagedisplay"/></a>
												<a class="butNav next" href="#">Next &raquo;</a>
												<a class="butNav last" href="#">Last &raquo;</a>
											</div>
											<label for="sizefiltro" >Mostrar datos:</label>
											<select id="sizefiltro" class="pagesize" >
											    
												
												<%
												if(iNumPlanificaciones>=10)
												    Response.Write('<option value="10">10</option>')
												if(iNumPlanificaciones>=20)
												    Response.Write('<option value="20">20</option>')
												if(iNumPlanificaciones>=30)
												    Response.Write('<option value="30">30</option>')    
												if(iNumPlanificaciones>=40)
												    Response.Write('<option value="40">40</option>')    
												if(iNumPlanificaciones!=10 && iNumPlanificaciones!=20 && iNumPlanificaciones!=30 && iNumPlanificaciones!=40)    
												{
												%>
    												<option  value="<%=iNumPlanificaciones%>"><%=iNumPlanificaciones%></option>
    										    <%
    										    }
    										    %>
											</select>
											</fieldset>
										</form>
									</div><!-- pager -->
								</div><!-- barbottom -->
							</div><!-- tab1 -->
							
							<div class="tab2" style="display:none;">
    							<form name="Form1" id="Form1" method=post>
                                <input type=hidden name="IdPlanificacion" id="IdPlanificacion">    
                                <input type=hidden name="TipoBusqueda" id="TipoBusqueda" value="S">    
								<!--  Tab 2 content -->
								<div class="fields clearfix">
									<h2>Rellena los datos del formulario para buscar</h2>
									<p class="sep">
										<label class="medium" for="nombre">Planificacion</label>
										<input type="text" value="<%=Planificacion%>" class="sText" id="Planificacion" name="Planificacion"/>
									</p>
									
								
								</div><!-- fields -->
								<div class="boxBut"><input type="submit" value="Buscar" class="butDef" /></div><!-- boxBut -->
								<div class="boxButfiltro"><input  id="clearfilter" value="Eliminar Filtros" class="butDef" /></div>
	                            </form>							
							</div><!-- tab2 -->
							
							<div class="tab3" style="display:none;">
								<!--  Tab 3 content -->
									<div class="fields clearfix">
									<form name="Form3" id="Form3" method=post target="frEjecutar" action="Planificacionesalta.asp" >
									    <input type=hidden name="strUnicoImagen1IDN" id="strUnicoImagen1IDN" value=""/>
										<h2>Rellena los datos del formulario para dar de alta una nueva Planificación
										<a href="javascript:CerrarNuevo()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Planificacion</label>
											<input type="text" value="" class="sText large" id="strUnicoPlanificacionN" name="strUnicoPlanificacionN"/>
										</p>
										
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrdenN" name="strUnicoOrdenN">
												<%
												    DibujarOrden('')
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivoN" name="strUnicoActivoN">
												<%
												DibujarActivadosBinario('')
												%>
											</select>
										</p>
										
										
										
									
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarNuevo()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Enviar" class="butDef" onclick="javascript:EnviarAlta()"/>
									</div><!-- fright -->
									</form>
							</div><!-- tab3-->
							<div class="tab4" style="display:none;">
								
								<form name="Form2" id="Form2" method=post target="frEjecutar" action="Planificacionesguardar.asp"  >
								    <input type=hidden name="strUnicoImagen1ID" id="strUnicoImagen1ID" value=""/>
									<div class="fields clearfix">
									<% 
										
										
										var strUnicoIdPlanificacion=""
                                         var strUnicoPlanificacion=""
                                        var strUnicoActivo=""
                                        var strUnicoOrden=1;
                                        if(IdPlanificacion!='')
                                        {
                                            var strSelect=" select * from Planificaciones where IdPlanificacion='"+IdPlanificacion+"' "
                                            
                                            MiRecordset.Open(strSelect,Conexion,3,1);
                                            
                                            if(!MiRecordset.EOF)
                                            {
                                                strUnicoIdPlanificacion=""+MiRecordset("IdPlanificacion")
                                                strUnicoPlanificacion=""+MiRecordset("Planificacion")
                                                strUnicoOrden = "" + MiRecordset("orden")
                                                strUnicoActivo=""+MiRecordset("Activo")
                                                
								            }
								            MiRecordset.Close()
								      	
								        }
								        
								        //Response.Write("//"+strUnicoTipo)
										%>
										<input type=hidden name="strUnicoIdPlanificacion" id="strUnicoIdPlanificacion" value="<%=strUnicoIdPlanificacion%>"/>
										<h2>Modifica los datos del formulario para editar la Planificación
										<a href="javascript:CerrarEditar()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Planificacion</label>
											<input type="text"  class="sText large" id="strUnicoPlanificacion" name="strUnicoPlanificacion" value="<%=strUnicoPlanificacion%>"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrden" name="strUnicoOrden">
												<%
												    DibujarOrden(strUnicoOrden)
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivo" name="strUnicoActivo">
												<%
												DibujarActivadosBinario(strUnicoActivo)
												%>
											</select>
										</p>
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarEditar()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Guardar" class="butDef" onclick="javascript:EnviarEditar()"/>
									</div><!-- fright-->
									</form>
							</div><!-- tab4-->
						</div><!-- tabContent-->
					</div><!-- tabbar-->
					<!--  PAGE CONTENT  -->
				</div><!-- content -->
			</div><!-- page -->
		</div><!-- container -->
		</div><!-- bk -->
		
		
		 
<iframe id="frEjecutar" name="frEjecutar" width="1" height="1" frameborder="0" ></iframe>

	</body>
</html>
<script type="text/javascript">

    $(function() {
        $('#clearfilter').click(function() {
            document.getElementById('frEjecutar').src = "includes/limpiarfiltros.asp"
        });
    });
IdPlanificacion='<%=IdPlanificacion%>'


if(IdPlanificacion!='')
showOnly('tab4','Planificaciones')

$(function(){
    $("#table") 
    .tablesorter({widthFixed: false, widgets: ['zebra']}) 
    .tablesorterPager({container: $("#pager")});   

});

function EditarPlanificacion(IdPlanificacion)
{
    document.getElementById('IdPlanificacion').value=IdPlanificacion
    document.getElementById('Form1').submit()
}
function CerrarEditar()
{
    document.getElementById('tab4').style.display="none"
    document.getElementById('tab3').style.display="block"
    showOnly('tab1','Planificaciones')
    $(".tabbar .tabs a").removeClass('selected');
	$("#a1").addClass("selected");
     
}
function EnviarEditar()
{
    
    

    if (Trim(document.getElementById('strUnicoPlanificacion').value) == null || Trim(document.getElementById('strUnicoPlanificacion').value) == "")
    {
        alert ("Campo Planificacion obligatorio");
        document.getElementById('strUnicoPlanificacion').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoOrden').value) == null || Trim(document.getElementById('strUnicoOrden').value) == "") {
        alert("Campo Orden obligatorio");
        document.getElementById('strUnicoOrden').focus()
        return;
    }
        
    
     if (Trim(document.getElementById('strUnicoActivo').value) == null || Trim(document.getElementById('strUnicoActivo').value) == "")
    {
        alert("Campo Activo obligatorio");
        document.getElementById('strUnicoActivo').focus()
        return;
    }
    
    
    if (!Confirmar("¿Seguro que quiere modificar estos datos?"))
    return

    document.getElementById('Form2').submit()
}
function EnviarAlta()
{
	
	if (Trim(document.getElementById('strUnicoPlanificacionN').value) == null || Trim(document.getElementById('strUnicoPlanificacionN').value) == "")
    {
        alert ("Campo Planificacion obligatorio");
        document.getElementById('strUnicoPlanificacionN').focus()
        return;
    }

    if (Trim(document.getElementById('strUnicoOrdenN').value) == null || Trim(document.getElementById('strUnicoOrdenN').value) == "") {
        alert("Campo Orden obligatorio");
        document.getElementById('strUnicoOrdenN').focus()
        return;
    }   
    
     if (Trim(document.getElementById('strUnicoActivoN').value) == null || Trim(document.getElementById('strUnicoActivoN').value) == "")
    {
        alert ("Campo Activo obligatorio");
        document.getElementById('strUnicoActivoN').focus()
        return;
    }
     
   
    

    if (!Confirmar("¿Seguro que quiere dar de alta una Planificación con estos datos?"))
        return

    document.getElementById('Form3').submit()
}
function CerrarNuevo()
{
    showOnly('tab1','Planificaciones')
    
   	$(".tabbar .tabs a").removeClass('selected');
    $("#a1").addClass("selected");
 
}
</script>
<%
cerrarBaseDatos()
%>
