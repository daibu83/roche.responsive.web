﻿<%@ Language=JScript codepage="65001" %>
<!--#include file ="includes/seguridad.asp"-->
<!--#include file ="includes/variables.asp"-->
<!--#include file ="inicio.asp"-->
<!--#include file ="includes/funciones.asp"-->
<!--#include file ="includes/funcionesbasedatos.asp"-->
<!--#include file ="includes/buscadores.asp"-->
<%
abrirBaseDatos()
var IdActividad=Request2("IdActividad")


var strTextoBusqueda=""

if(Actividad !='')
    strTextoBusqueda += "Actividad : " + Actividad + ". "

if (IdCategoriaBuscar != '')
    strTextoBusqueda += "Categoría : " + GetCategoria(IdCategoriaBuscar) + ". "    

bOperar = false;
bOperar = GetOperar();
    
%>
		<div id="bk">
		<!--#include file ="header.asp"-->
		<!--#include file ="tooltip.asp"-->
		<div id="container" class="clearfix">
			  <!--#include file ="menu.asp"-->
			<div id="page">
				<div class="menu clearfix">
					 <!-- PAGE TITLE --> 
					<div>Actividades</div>
					<div class="cr_pass"></div>
				</div><!-- menu -->
				
				 <!-- PAGE CONTENT --> 
				<div class="clearfix content">
					 <!-- Tab bar component --> 
					<div class="clearfix tabbar Actividades">
						<ul class="tabs">
							<li class="tab1" id="tab1">
							<%
							
							var Ocultartab3=' style="display:none" '
							var Ocultartab4=' style="display:none" '
							var Seleccionartab1=' class="selected" '
							var Seleccionartab4=' class="selected" '
							if(IdActividad!='')
							{
							    Ocultartab4=""
							    Seleccionartab1=""
							}
							else
							{
							    Ocultartab3=""
							    Seleccionartab4=""
							}
							
							%>
								<a id="a1" <%=Seleccionartab1%> onclick="showOnly('tab1','Actividades')" >
									Listado
								</a>
							</li>
							<li class="tab2" id="tab2">
								<a id="a2" onclick="showOnly('tab2','Actividades')">
									<img src="img/icons/magnifier.png" alt=""/>
									Buscador
								</a>
							</li>
							<li class="tab3" <%=Ocultartab3%> id="tab3">
								<a id="a3" onclick="showOnly('tab3','Actividades')">
									<img src="img/icons/plus-small.png" alt=""/>
									Actividad Nueva
								</a>
							</li>
							<li class="tab4" <%=Ocultartab4%> id="tab4">
								<a id="a4" <%=Seleccionartab4%> onclick="showOnly('tab4','Actividades')">
									<img src="img/icons/edit.png" alt=""/>
									Ver editar
								</a>
							</li>
						</ul>
						<div class="tabContent clearfix">
						<!-- tab 1 content -->
							<div class="tab1">
								<%
								if(strTextoBusqueda!='')
								{
								%>
									<div class="box-padd">
									<p class="text filtrored">Atención, tienes filtros de búsqueda aplicados. Resultados de la búsqueda. <%=strTextoBusqueda%><strong></strong></p>
								</div><!-- box-padd -->
								<%
						        }
						        %>
								<table cellpadding="0" cellspacing="0" border="0" id="table"  class="uiTable tablesorter">  
								
									<thead> 
										<tr>
											<th><h3>Actividad</h3></th> 
											<th><h3>Categoria</h3></th> 
											<th><h3>Orden</h3></th> 
											<th><h3>Pasos</h3></th> 
											<th><h3>Activo</h3></th> 
											<th><h3>Editar</h3></th>
											<!--
											<th><h3>Eliminar</h3></th> 											
											-->
										</tr> 
									</thead> 
									<tbody> 
									
										<%
										
									var Msql="select Actividades.*,actividades_categorias.categoria  from Actividades inner join actividades_categorias on actividades_categorias.idcategoria=Actividades.idcategoria where IdActividad>0  "
									if(Actividad!='')
									    Msql+=" and Actividad like '%"+Actividad+"%' "
									if(IdCategoriaBuscar!='')
									    Msql+=" and Actividades.idcategoria = '"+IdCategoriaBuscar+"' "
									Msql+=" order by idcategoria,orden "
                                    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    								MiRecordset.Open(Msql,Conexion,3,1);
                                    var iNumActividades=0
                                     if(MiRecordset.EOF)
                                    {
                                        Response.Write('<tr><td colspan="4">No hay datos para el criterio de busqueda</td></tr>')
                                    }
                                    while(!MiRecordset.EOF)
                                    {
                                        strActividad=""+MiRecordset("Actividad")
                                        strIdActividad=""+MiRecordset("IdActividad")
                                        strCategoria=""+MiRecordset("Categoria")
                                        strActivo=""+MiRecordset("Activo")
                                        strOrden=""+MiRecordset("Orden")
                                        strIcono="minus-circle.png"
                                        if(strActivo==1)
                                            strIcono="tick.png"
                                        select="select paso,idpaso from Actividadespasos where IdActividad="+strIdActividad +" order by orden "
                                        strPasos="";
                                         var MiRecordset1 =  Server.CreateObject("ADODB.Recordset");
    								    MiRecordset1.Open(select,Conexion,3,1);
    								    while(!MiRecordset1.EOF)
                                        {
                                            
                                            strPasos+="<a style='text-decoration:underline'  href='Actividadespasos.asp?TipoBusqueda=S&IdPaso="+String(MiRecordset1('idpaso'))+"&IdActividadbuscar="+strIdActividad+"' >"+String(MiRecordset1('paso'))+"</a><br/>"
                                            MiRecordset1.MoveNext();
                                        }
                                        MiRecordset1.Close();
                                        
                                        
                                            
                                        
                                        
									%>
									
										<tr> 
											<td style="width:200px"><%=strActividad%></td> 
											<td style="width:200px"><%=strCategoria%></td> 
											<td style="width:200px"><%=strOrden%></td> 
											<td style="width:200px"><%=strPasos%></td> 
											
											<td><img src="img/icons/<%=strIcono%>" class="icon" alt="Actividades"/></td> 

											<td><a href="javascript:EditarActividad('<%=strIdActividad%>')" title="Editar"><img src="img/icons/edit.png" alt="Editar"/></a></td>  
											
										</tr> 
									<%
									    MiRecordset.MoveNext()
									    iNumActividades++
									}
									MiRecordset.Close()
									%>
																		
									</tbody> 
								</table>
								 <!-- Navigation controls --> 
								<br class="clear" />
								<div class="barbottom">
									<!-- pagination -->
									<div id="pager" class="pager">
										<form action="#">
										<fieldset>
											<div class="fright">
												<a class="butNav first" href="#">&laquo; First</a>
												<a class="butNav prev" href="#">&laquo; Previous</a>
												<a class="butPage" href="#"><input type="text" class="pagedisplay"/></a>
												<a class="butNav next" href="#">Next &raquo;</a>
												<a class="butNav last" href="#">Last &raquo;</a>
											</div>
											<label for="sizefiltro" >Mostrar datos:</label>
											<select id="sizefiltro" class="pagesize" >
											    
												
												<%
												if(iNumActividades>=10)
												    Response.Write('<option value="10">10</option>')
												if(iNumActividades>=20)
												    Response.Write('<option value="20">20</option>')
												if(iNumActividades>=30)
												    Response.Write('<option value="30">30</option>')    
												if(iNumActividades>=40)
												    Response.Write('<option value="40">40</option>')    
												if(iNumActividades!=10 && iNumActividades!=20 && iNumActividades!=30 && iNumActividades!=40)    
												{
												%>
    												<option  value="<%=iNumActividades%>"><%=iNumActividades%></option>
    										    <%
    										    }
    										    %>
											</select>
											</fieldset>
										</form>
									</div><!-- pager -->
								</div><!-- barbottom -->
							</div><!-- tab1 -->
							
							<div class="tab2" style="display:none;">
    							<form name="Form1" id="Form1" method=post>
                                <input type=hidden name="IdActividad" id="IdActividad">    
                                <input type=hidden name="TipoBusqueda" id="TipoBusqueda" value="S">    
								<!--  Tab 2 content -->
								<div class="fields clearfix">
									<h2>Rellena los datos del formulario para buscar</h2>
									<p class="sep">
										<label class="medium" for="nombre">Actividad</label>
										<input type="text" value="<%=Actividad%>" class="sText" id="Actividad" name="Actividad"/>
									</p>
									<p class="sep">
											<label class="medium" for="nombreAdd1">Categoría</label>
											<select class="sSelect large" id="IdCategoriaBuscar" name="IdCategoriaBuscar">
												<%
												    DibujarCategorias(IdCategoriaBuscar)
												%>
											</select>
									</p>
									
								
								</div><!-- fields -->
								<div class="boxBut"><input type="submit" value="Buscar" class="butDef" /></div><!-- boxBut -->
								<div class="boxButfiltro"><input  id="clearfilter" value="Eliminar Filtros" class="butDef" /></div>
	                            </form>							
							</div><!-- tab2 -->
							
							<div class="tab3" style="display:none;">
								<!--  Tab 3 content -->
									<div class="fields clearfix">
									<form name="Form3" id="Form3" method=post target="frEjecutar" action="Actividadesalta.asp" >
									    <input type=hidden name="strUnicoImagen1IDN" id="strUnicoImagen1IDN" value=""/>
										<h2>Rellena los datos del formulario para dar de alta una nueva Planificación
										<a href="javascript:CerrarNuevo()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Título corto</label>
											<input type="text" value="" class="sText large" id="strUnicoTituloCortoN" name="strUnicoTituloCortoN"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Actividad</label>
											<input type="text" value="" class="sText large" id="strUnicoActividadN" name="strUnicoActividadN"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Categoría</label>
											<select class="sSelect large" id="strUnicoIdCategoriaN" name="strUnicoIdCategoriaN">
												<%
												    DibujarCategorias('')
												%>
											</select>
									    </p>
									    <p class="sep">
											<label class="medium" for="nombreAdd1">Responsable</label>
											<select class="sSelect large" id="strUnicoResponsableN" name="strUnicoResponsableN">
												<%
												    DibujarResponsables('')
												%>
											</select>
									    </p>
									    <p class="sep">
											<label class="medium" for="nombreAdd1">Logística</label>
											<input type="text" value="" class="sText large" id="strUnicoLogisticaN" name="strUnicoLogisticaN"/>
										</p>
									    <div class="sep">
											<label class="medium" for="nombreAdd1">Tipo Evento</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="15" id="strUnicoTipoEventoN" name="strUnicoTipoEventoN"></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Ejemplos</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="15" id="strUnicoEjemplosN" name="strUnicoEjemplosN"></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Aspectos a tener encuenta</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="15" id="strUnicoAspectosN" name="strUnicoAspectosN"></textarea>
											</div>
										</div>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrdenN" name="strUnicoOrdenN">
												<%
												    DibujarOrden('')
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivoN" name="strUnicoActivoN">
												<%
												DibujarActivadosBinario('')
												%>
											</select>
										</p>
										
										
										
									
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarNuevo()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Enviar" class="butDef" onclick="javascript:EnviarAlta()"/>
									</div><!-- fright -->
									</form>
							</div><!-- tab3-->
							<div class="tab4" style="display:none;">
								
								<form name="Form2" id="Form2" method=post target="frEjecutar" action="Actividadesguardar.asp"  >
								    <input type=hidden name="strUnicoImagen1ID" id="strUnicoImagen1ID" value=""/>
									<div class="fields clearfix">
									<% 
										
										
										var strUnicoIdActividad=""
                                         var strUnicoActividad=""
                                        var strUnicoActivo=""
                                        var strUnicoOrden = 1;
                                        var strUnicoResponsable = ""
                                        var strUnicoLogistica = ""
                                        var strUnicoTipoEvento = ""
                                        var strUnicoEjemplos = ""
                                        var strUnicoAspectos = ""
                                        var strUnicoIdCategoria = ""
                                        var strUnicoTituloCorto = ""
                                        if(IdActividad!='')
                                        {
                                            var strSelect=" select * from Actividades where IdActividad='"+IdActividad+"' "
                                            
                                            MiRecordset.Open(strSelect,Conexion,3,1);
                                            
                                            if(!MiRecordset.EOF) {
                                                strUnicoResponsable = "" + MiRecordset("responsable")
                                                strUnicoLogistica = "" + MiRecordset("logistica")
                                                strUnicoTipoEvento = "" + MiRecordset("tipoevento")
                                                strUnicoEjemplos = "" + MiRecordset("ejemplos")
                                                strUnicoAspectos = "" + MiRecordset("aspectos")
                                                strUnicoIdCategoria = "" + MiRecordset("idcategoria")
                                                strUnicoTituloCorto = "" + MiRecordset("titulocorto")
                                            
                                                strUnicoIdActividad=""+MiRecordset("IdActividad")
                                                strUnicoActividad=""+MiRecordset("Actividad")
                                                strUnicoOrden = "" + MiRecordset("orden")
                                                strUnicoActivo=""+MiRecordset("Activo")
                                                
								            }
								            MiRecordset.Close()
								      	
								        }
								        
								        //Response.Write("//"+strUnicoTipo)
										%>
										<input type=hidden name="strUnicoIdActividad" id="strUnicoIdActividad" value="<%=strUnicoIdActividad%>"/>
										<h2>Modifica los datos del formulario para editar la Planificación
										<a href="javascript:CerrarEditar()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Título corto</label>
											<input type="text" value="<%=strUnicoTituloCorto%>" class="sText large" id="strUnicoTituloCorto" name="strUnicoTituloCorto"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Actividad</label>
											<input type="text" value="<%=strUnicoActividad%>" class="sText large" id="strUnicoActividad" name="strUnicoActividad"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Categoría</label>
											<select class="sSelect large" id="strUnicoIdCategoria" name="strUnicoIdCategoria">
												<%
												    DibujarCategorias(strUnicoIdCategoria)
												%>
											</select>
									    </p>
									    <p class="sep">
											<label class="medium" for="nombreAdd1">Responsable</label>
											<select class="sSelect large" id="strUnicoResponsable" name="strUnicoResponsable">
												<%
												    DibujarResponsables(strUnicoResponsable)
												%>
											</select>
									    </p>
									    <p class="sep">
											<label class="medium" for="nombreAdd1">Logística</label>
											<input type="text" value="<%=strUnicoLogistica%>" class="sText large" id="strUnicoLogistica" name="strUnicoLogistica"/>
										</p>
									    <div class="sep">
											<label class="medium" for="nombreAdd1">Tipo Evento</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="15" id="strUnicoTipoEvento" name="strUnicoTipoEvento"><%=strUnicoTipoEvento%></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Ejemplos</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="15" id="strUnicoEjemplos" name="strUnicoEjemplos"><%=strUnicoEjemplos%></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Aspectos a tener encuenta</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="15" id="strUnicoAspectos" name="strUnicoAspectos"><%=strUnicoAspectos%></textarea>
											</div>
										</div>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrden" name="strUnicoOrden">
												<%
												    DibujarOrden(strUnicoOrden)
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivo" name="strUnicoActivo">
												<%
												DibujarActivadosBinario(strUnicoActivo)
												%>
											</select>
										</p>
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarEditar()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Grabar" class="butDef" onclick="javascript:EnviarEditar()"/>
									</div><!-- fright-->
									</form>
							</div><!-- tab4-->
						</div><!-- tabContent-->
					</div><!-- tabbar-->
					<!--  PAGE CONTENT  -->
				</div><!-- content -->
			</div><!-- page -->
		</div><!-- container -->
		</div><!-- bk -->
		
		
		 
<iframe id="frEjecutar" name="frEjecutar" width="1" height="1" frameborder="0" ></iframe>

	</body>
</html>
<script type="text/javascript">

    $(function() {
        $('#clearfilter').click(function() {
            document.getElementById('frEjecutar').src = "includes/limpiarfiltros.asp"
        });
    });

IdActividad='<%=IdActividad%>'


if(IdActividad!='')
showOnly('tab4','Actividades')

$(function(){
    $("#table") 
    .tablesorter({widthFixed: false, widgets: ['zebra']}) 
    .tablesorterPager({container: $("#pager")});   

});

function EditarActividad(IdActividad)
{
    document.getElementById('IdActividad').value=IdActividad
    document.getElementById('Form1').submit()
}
function CerrarEditar()
{
    document.getElementById('tab4').style.display="none"
    document.getElementById('tab3').style.display="block"
    showOnly('tab1','Actividades')
    $(".tabbar .tabs a").removeClass('selected');
	$("#a1").addClass("selected");
     
}
function EnviarEditar()
{



    if (Trim(document.getElementById('strUnicoTituloCorto').value) == null || Trim(document.getElementById('strUnicoTituloCorto').value) == "") {
        alert("Campo Título corto obligatorio");
        document.getElementById('strUnicoTituloCorto').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoActividad').value) == null || Trim(document.getElementById('strUnicoActividad').value) == "") {
        alert("Campo Actividad obligatorio");
        document.getElementById('strUnicoActividad').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoIdCategoria').value) == null || Trim(document.getElementById('strUnicoIdCategoria').value) == "") {
        alert("Campo Categoría obligatoria");
        document.getElementById('strUnicoIdCategoria').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoResponsable').value) == null || Trim(document.getElementById('strUnicoResponsable').value) == "") {
        alert("Campo Responsable obligatorio");
        document.getElementById('strUnicoResponsable').focus()
        return;
    }

    if (Trim(document.getElementById('strUnicoLogistica').value) == null || Trim(document.getElementById('strUnicoLogistica').value) == "") {
        alert("Campo Logística obligatorio");
        document.getElementById('strUnicoLogistica').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoOrden').value) == null || Trim(document.getElementById('strUnicoOrden').value) == "") {
        alert("Campo Orden obligatorio");
        document.getElementById('strUnicoOrden').focus()
        return;
    }
        
    
     if (Trim(document.getElementById('strUnicoActivo').value) == null || Trim(document.getElementById('strUnicoActivo').value) == "")
    {
        alert("Campo Activo obligatorio");
        document.getElementById('strUnicoActivo').focus()
        return;
    }
    
    
    if (!Confirmar("¿Seguro que quiere modificar estos datos?"))
    return

    document.getElementById('Form2').submit()
}
function EnviarAlta()
{


    if (Trim(document.getElementById('strUnicoTituloCortoN').value) == null || Trim(document.getElementById('strUnicoTituloCortoN').value) == "") {
        alert("Campo Título corto obligatorio");
        document.getElementById('strUnicoTituloCortoN').focus()
        return;
    }
	if (Trim(document.getElementById('strUnicoActividadN').value) == null || Trim(document.getElementById('strUnicoActividadN').value) == "")
    {
        alert ("Campo Actividad obligatorio");
        document.getElementById('strUnicoActividadN').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoIdCategoriaN').value) == null || Trim(document.getElementById('strUnicoIdCategoriaN').value) == "") {
        alert("Campo Categoría obligatoria");
        document.getElementById('strUnicoIdCategoriaN').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoResponsableN').value) == null || Trim(document.getElementById('strUnicoResponsableN').value) == "") {
        alert("Campo Responsable obligatorio");
        document.getElementById('strUnicoResponsableN').focus()
        return;
    }

    if (Trim(document.getElementById('strUnicoLogisticaN').value) == null || Trim(document.getElementById('strUnicoLogisticaN').value) == "") {
        alert("Campo Logística obligatorio");
        document.getElementById('strUnicoLogisticaN').focus()
        return;
    }
    
    if (Trim(document.getElementById('strUnicoOrdenN').value) == null || Trim(document.getElementById('strUnicoOrdenN').value) == "") {
        alert("Campo Orden obligatorio");
        document.getElementById('strUnicoOrdenN').focus()
        return;
    }   
    
     if (Trim(document.getElementById('strUnicoActivoN').value) == null || Trim(document.getElementById('strUnicoActivoN').value) == "")
    {
        alert ("Campo Activo obligatorio");
        document.getElementById('strUnicoActivoN').focus()
        return;
    }
     
   
    

    if (!Confirmar("¿Seguro que quiere dar de alta una Planificación con estos datos?"))
        return

    document.getElementById('Form3').submit()
}
function CerrarNuevo()
{
    showOnly('tab1','Actividades')
    
   	$(".tabbar .tabs a").removeClass('selected');
    $("#a1").addClass("selected");

}
tinymce.init({
    mode: "textareas",

    theme_advanced_buttons1: "link, unlink,",
    force_br_newlines: true,
    force_p_newlines: false,
    forced_root_block: false,
    plugins: "paste,link,code",
    cleanup: false,
    verify_html: false


});
</script>
<%
cerrarBaseDatos()
%>
