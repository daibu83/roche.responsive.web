﻿<%@ Language=JScript codepage="65001" %>
<!--#include file ="includes/seguridad.asp"-->
<!--#include file ="includes/variables.asp"-->
<!--#include file ="inicio.asp"-->
<!--#include file ="includes/funciones.asp"-->
<!--#include file ="includes/funcionesbasedatos.asp"-->
<!--#include file ="includes/buscadores.asp"-->
<%
    abrirBaseDatos()
    var IdPaso = Request2("IdPaso")

    var strTextoBusqueda = ""

    if (Paso != '')
        strTextoBusqueda += "Paso : " + Paso + ". "

    if (IdPlanificacionbuscar != '')
        strTextoBusqueda += "Planificación : " + GetPlanificacion(IdPlanificacionbuscar) + ". "

    bOperar = false;
    bOperar = GetOperar();
    
%>
		<div id="bk">
		<!--#include file ="header.asp"-->
		<!--#include file ="tooltip.asp"-->
		<div id="container" class="clearfix">
			  <!--#include file ="menu.asp"-->
			<div id="page">
				<div class="menu clearfix">
					 <!-- PAGE TITLE --> 
					<div>Pasos Planificaciones</div>
					<div class="cr_pass"></div>
				</div><!-- menu -->
				
				 <!-- PAGE CONTENT --> 
				<div class="clearfix content">
					 <!-- Tab bar component --> 
					<div class="clearfix tabbar Planificacionespasos">
						<ul class="tabs">
							<li class="tab1" id="tab1">
							<%

							    var Ocultartab3 = ' style="display:none" '
							    var Ocultartab4 = ' style="display:none" '
							    var Seleccionartab1 = ' class="selected" '
							    var Seleccionartab4 = ' class="selected" '
							    if (IdPaso != '') {
							        Ocultartab4 = ""
							        Seleccionartab1 = ""
							    }
							    else {
							        Ocultartab3 = ""
							        Seleccionartab4 = ""
							    }
							
							%>
								<a id="a1" <%=Seleccionartab1%> onclick="showOnly('tab1','Planificacionespasos')" >
									Listado
								</a>
							</li>
							<li class="tab2" id="tab2">
								<a id="a2" onclick="showOnly('tab2','Planificacionespasos')">
									<img src="img/icons/magnifier.png" alt=""/>
									Buscador
								</a>
							</li>
							<li class="tab3" <%=Ocultartab3%> id="tab3">
								<a id="a3" onclick="showOnly('tab3','Planificacionespasos')">
									<img src="img/icons/plus-small.png" alt=""/>
									Pasos Nuevo
								</a>
							</li>
							<li class="tab4" <%=Ocultartab4%> id="tab4">
								<a id="a4" <%=Seleccionartab4%> onclick="showOnly('tab4','Planificacionespasos')">
									<img src="img/icons/edit.png" alt=""/>
									Ver editar
								</a>
							</li>
						</ul>
						<div class="tabContent clearfix">
						<!-- tab 1 content -->
							<div class="tab1">
								<%
								if(strTextoBusqueda!='')
								{
								%>
									<div class="box-padd">
									<p class="text filtrored">Atención, tienes filtros de búsqueda aplicados. Resultados de la búsqueda. <%=strTextoBusqueda%><strong></strong></p>
								</div><!-- box-padd -->
								<%
						        }
						        %>
								<table cellpadding="0" cellspacing="0" border="0" id="table"  class="uiTable tablesorter">  
								
									<thead> 
										<tr>
										    <th><h3>Planificacion</h3></th>
											<th><h3>Paso</h3></th> 
											<th><h3>Orden</h3></th> 
											<th><h3>Activo</h3></th> 
											<th><h3>Editar</h3></th>
											<!--
											<th><h3>Eliminar</h3></th> 											
											-->
										</tr> 
									</thead> 
									<tbody> 
									
										<%
									var Msql="select Planificacionespasos.*,planificaciones.planificacion  from Planificacionespasos inner join planificaciones on planificaciones.idplanificacion= Planificacionespasos.idplanificacion where IdPaso>0  "
									if(Paso!='')
									    Msql+=" and substring(Paso,0,1000) like '%"+Paso+"%' "
									if(IdPlanificacionbuscar!='')
									    Msql+=" and Planificacionespasos.IdPlanificacion ='"+IdPlanificacionbuscar+"' "
									Msql+=" order by Planificaciones.orden,Planificacionespasos.orden "
                                    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    								MiRecordset.Open(Msql,Conexion,3,1);
                                    var iNumPlanificacionespasos=0
                                     if(MiRecordset.EOF)
                                    {
                                        Response.Write('<tr><td colspan="4">No hay datos para el criterio de busqueda</td></tr>')
                                    }
                                    while(!MiRecordset.EOF)
                                    {
                                        strPaso=""+MiRecordset("Paso")
                                        strIdPaso=""+MiRecordset("IdPaso")
                                        strActivo=""+MiRecordset("Activo")
                                        strOrden=""+MiRecordset("Orden")
                                        strIcono="minus-circle.png"
                                        if(strActivo==1)
                                            strIcono="tick.png"
                                        strPlanificacion=""+MiRecordset("Planificacion")
                                        
                                        
									%>
									
										<tr>
										    <td style="width:200px"><%=strPlanificacion%></td>  
											<td style="width:200px"><%=strPaso%></td> 
											<td style="width:200px"><%=strOrden%></td> 
											<td><img src="img/icons/<%=strIcono%>" class="icon" alt="Planificacionespasos"/></td> 
											<td><a href="javascript:EditarPaso('<%=strIdPaso%>')" title="Editar"><img src="img/icons/edit.png" alt="Editar"/></a></td>  
											
										</tr> 
									<%
									    MiRecordset.MoveNext()
									    iNumPlanificacionespasos++
									}
									MiRecordset.Close()
									%>
																		
									</tbody> 
								</table>
								 <!-- Navigation controls --> 
								<br class="clear" />
								<div class="barbottom">
									<!-- pagination -->
									<div id="pager" class="pager">
										<form action="#">
										<fieldset>
											<div class="fright">
												<a class="butNav first" href="#">&laquo; First</a>
												<a class="butNav prev" href="#">&laquo; Previous</a>
												<a class="butPage" href="#"><input type="text" class="pagedisplay"/></a>
												<a class="butNav next" href="#">Next &raquo;</a>
												<a class="butNav last" href="#">Last &raquo;</a>
											</div>
											<label for="sizefiltro" >Mostrar datos:</label>
											<select id="sizefiltro" class="pagesize" >
											    
												
												<%
												if(iNumPlanificacionespasos>=10)
												    Response.Write('<option value="10">10</option>')
												if(iNumPlanificacionespasos>=20)
												    Response.Write('<option value="20">20</option>')
												if(iNumPlanificacionespasos>=30)
												    Response.Write('<option value="30">30</option>')    
												if(iNumPlanificacionespasos>=40)
												    Response.Write('<option value="40">40</option>')    
												if(iNumPlanificacionespasos!=10 && iNumPlanificacionespasos!=20 && iNumPlanificacionespasos!=30 && iNumPlanificacionespasos!=40)    
												{
												%>
    												<option  value="<%=iNumPlanificacionespasos%>"><%=iNumPlanificacionespasos%></option>
    										    <%
    										    }
    										    %>
											</select>
											</fieldset>
										</form>
									</div><!-- pager -->
								</div><!-- barbottom -->
							</div><!-- tab1 -->
							
							<div class="tab2" style="display:none;">
    							<form name="Form1" id="Form1" method=post action="planificacionespasos.asp" >
                                <input type=hidden name="IdPaso" id="IdPaso">    
                                <input type=hidden name="TipoBusqueda" id="TipoBusqueda" value="S">    
								<!--  Tab 2 content -->
								<div class="fields clearfix">
									<h2>Rellena los datos del formulario para buscar</h2>
									<p class="sep">
										<label class="medium" for="nombre">Paso</label>
										<input type="text" value="<%=Paso%>" class="sText" id="Paso" name="Paso"/>
									</p>
									<p class="sep">
											<label class="medium" for="nombreAdd1">Planificación</label>
											<select class="sSelect large" id="IdPlanificacionbuscar" name="IdPlanificacionbuscar">
												<%
												    DibujarPlanificaciones(IdPlanificacionbuscar)
												%>
											</select>
										</p>
									
								
								</div><!-- fields -->
								<div class="boxBut"><input type="submit" value="Buscar" class="butDef" /></div><!-- boxBut -->
								<div class="boxButfiltro"><input  id="clearfilter" value="Eliminar Filtros" class="butDef" /></div>
	                            </form>							
							</div><!-- tab2 -->
							
							<div class="tab3" style="display:none;">
								<!--  Tab 3 content -->
									<div class="fields clearfix">
									<form name="Form3" id="Form3" method=post target="frEjecutar" action="Planificacionespasosalta.asp" >
									    <input type=hidden name="strUnicoImagen1IDN" id="strUnicoImagen1IDN" value=""/>
										<h2>Rellena los datos del formulario para dar de alta un nuevo Paso
										<a href="javascript:CerrarNuevo()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Planificación</label>
											<select class="sSelect large" id="strUnicoIdPlanificacionN" name="strUnicoIdPlanificacionN">
												<%
												    DibujarPlanificaciones('')
												%>
											</select>
										</p>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Paso</label>
											<input type="text" value="" class="sText large" id="strUnicoPasoN" name="strUnicoPasoN"/>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Que</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="5" id="strUnicoQueN" name="strUnicoQueN"></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Quién</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="5" id="strUnicoQuienN" name="strUnicoQuienN"></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Donde</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="5" id="strUnicoDondeN" name="strUnicoDondeN"></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Cuando</label>
											<div class="divmce-tinymce">
											<textarea cols="70" rows="5" id="strUnicoCuandoN" name="strUnicoCuandoN"></textarea>
											</div>
										</div>
										
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrdenN" name="strUnicoOrdenN">
												<%
												    DibujarOrden('')
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivoN" name="strUnicoActivoN">
												<%
												    DibujarActivadosBinario('')
												%>
											</select>
										</p>
										
										
										
									
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarNuevo()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Enviar" class="butDef" onclick="javascript:EnviarAlta()"/>
									</div><!-- fright -->
									</form>
							</div><!-- tab3-->
							<div class="tab4" style="display:none;">
								
								<form name="Form2" id="Form2" method=post target="frEjecutar" action="Planificacionespasosguardar.asp"  >
								    <input type=hidden name="strUnicoImagen1ID" id="strUnicoImagen1ID" value=""/>
									<div class="fields clearfix">
									<%


									    var strUnicoIdPaso = ""
									    var strUnicoPaso = ""
									    var strUnicoActivo = ""
									    var strUnicoOrden = 1;
									    var strUnicoIdPlanificacion = ''
									    var strUnicoQue="";
									    var strUnicoQuien="";
									    var strUnicoDonde="";
									    var strUnicoCuando = "";
									    if (IdPaso != '') {
									        var strSelect = " select * from Planificacionespasos where IdPaso='" + IdPaso + "' "

									        MiRecordset.Open(strSelect, Conexion, 3, 1);

									        if (!MiRecordset.EOF) {
									            strUnicoIdPaso = "" + MiRecordset("IdPaso")
									            strUnicoPaso = "" + MiRecordset("Paso")
									            strUnicoQue = "" + MiRecordset("Que")
									            strUnicoQuien = "" + MiRecordset("Quien")
									            strUnicoDonde = "" + MiRecordset("Donde")
									            strUnicoCuando = "" + MiRecordset("Cuando")
									            strUnicoOrden = "" + MiRecordset("orden")
									            strUnicoActivo = "" + MiRecordset("Activo")
									            strUnicoIdPlanificacion = "" + MiRecordset("IdPlanificacion")
									        }
									        MiRecordset.Close()

									    }

									    //Response.Write("//"+strUnicoTipo)
										%>
										<input type=hidden name="strUnicoIdPaso" id="strUnicoIdPaso" value="<%=strUnicoIdPaso%>"/>
										<h2>Modifica los datos del formulario para editar la Planificación
										<a href="javascript:CerrarEditar()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Planificación</label>
											<select class="sSelect large" id="strUnicoIdPlanificacion" name="strUnicoIdPlanificacion">
												<%
												    DibujarPlanificaciones(strUnicoIdPlanificacion)
												%>
											</select>
										</p>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Paso</label>
											<input type="text" value="<%=strUnicoPaso%>" class="sText large" id="strUnicoPaso" name="strUnicoPaso"/>
										</div>
										
										<div class="sep">
											<label class="medium" for="nombreAdd1">Que</label>
											<div class="divmce-tinymce">
											<textarea cols="50" rows="5" id="strUnicoQue" name="strUnicoQue"><%=strUnicoQue%></textarea>
									        </div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Quién</label>
											<div class="divmce-tinymce">
											<textarea cols="50" rows="5" id="strUnicoQuien" name="strUnicoQuien"><%=strUnicoQuien%></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Donde</label>
											<div class="divmce-tinymce">
											<textarea cols="50" rows="5" id="strUnicoDonde" name="strUnicoDonde"><%=strUnicoDonde%></textarea>
											</div>
										</div>
										<div class="sep">
											<label class="medium" for="nombreAdd1">Cuando</label>
											<div class="divmce-tinymce">
											<textarea cols="50" rows="5" id="strUnicoCuando" name="strUnicoCuando"><%=strUnicoCuando%></textarea>
											</div>
										</div>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrden" name="strUnicoOrden">
												<%
												    DibujarOrden(strUnicoOrden)
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivo" name="strUnicoActivo">
												<%
												    DibujarActivadosBinario(strUnicoActivo)
												%>
											</select>
										</p>
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarEditar()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Guardar" class="butDef" onclick="javascript:EnviarEditar()"/>
									</div><!-- fright-->
									</form>
							</div><!-- tab4-->
						</div><!-- tabContent-->
					</div><!-- tabbar-->
					<!--  PAGE CONTENT  -->
				</div><!-- content -->
			</div><!-- page -->
		</div><!-- container -->
		</div><!-- bk -->
		
		
		 
<iframe id="frEjecutar" name="frEjecutar" width="1" height="1" frameborder="0" ></iframe>

	</body>
</html>
<script type="text/javascript">

    $(function() {
        $('#clearfilter').click(function() {
            document.getElementById('frEjecutar').src = "includes/limpiarfiltros.asp"
        });
    });
    IdPaso = '<%=IdPaso%>'


    if (IdPaso != '')
        showOnly('tab4', 'Planificacionespasos')

    $(function() {
        $("#table")
    .tablesorter({ widthFixed: false, widgets: ['zebra'] })
    .tablesorterPager({ container: $("#pager") });

    });

    function EditarPaso(IdPaso) {
        document.getElementById('IdPaso').value = IdPaso
        document.getElementById('Form1').submit()
    }
    function CerrarEditar() {
        document.getElementById('tab4').style.display = "none"
        document.getElementById('tab3').style.display = "block"
        showOnly('tab1', 'Planificacionespasos')
        $(".tabbar .tabs a").removeClass('selected');
        $("#a1").addClass("selected");

    }
    function EnviarEditar() {


        if (Trim(document.getElementById('strUnicoIdPlanificacion').value) == null || Trim(document.getElementById('strUnicoIdPlanificacion').value) == "") {
            alert("Campo Planificación obligatorio");
            document.getElementById('strUnicoIdPlanificacion').focus()
            return;
        }
        if (Trim(document.getElementById('strUnicoPaso').value) == null || Trim(document.getElementById('strUnicoPaso').value) == "") {
            alert("Campo Paso obligatorio");
            document.getElementById('strUnicoPaso').focus()
            return;
        }
        
        if (Trim(document.getElementById('strUnicoOrden').value) == null || Trim(document.getElementById('strUnicoOrden').value) == "") {
            alert("Campo Orden obligatorio");
            document.getElementById('strUnicoOrden').focus()
            return;
        }
        

        if (Trim(document.getElementById('strUnicoActivo').value) == null || Trim(document.getElementById('strUnicoActivo').value) == "") {
            alert("Campo Activo obligatorio");
            document.getElementById('strUnicoActivo').focus()
            return;
        }


        if (!Confirmar("¿Seguro que quiere modificar estos datos?"))
            return

        document.getElementById('Form2').submit()
    }
    function EnviarAlta() {
        if (Trim(document.getElementById('strUnicoIdPlanificacionN').value) == null || Trim(document.getElementById('strUnicoIdPlanificacionN').value) == "") {
            alert("Campo Planificación obligatorio");
            document.getElementById('strUnicoIdPlanificacionN').focus()
            return;
        }
        if (Trim(document.getElementById('strUnicoPasoN').value) == null || Trim(document.getElementById('strUnicoPasoN').value) == "") {
            alert("Campo Paso obligatorio");
            document.getElementById('strUnicoPasoN').focus()
            return;
        }
        if (Trim(document.getElementById('strUnicoOrdenN').value) == null || Trim(document.getElementById('strUnicoOrdenN').value) == "") {
            alert("Campo Orden obligatorio");
            document.getElementById('strUnicoOrdenN').focus()
            return;
        }
        
        if (Trim(document.getElementById('strUnicoActivoN').value) == null || Trim(document.getElementById('strUnicoActivoN').value) == "") {
            alert("Campo Activo obligatorio");
            document.getElementById('strUnicoActivoN').focus()
            return;
        }




        if (!Confirmar("¿Seguro que quiere dar de alta una Planificación con estos datos?"))
            return

        document.getElementById('Form3').submit()
    }
    function CerrarNuevo() {
        showOnly('tab1', 'Planificacionespasos')

        $(".tabbar .tabs a").removeClass('selected');
        $("#a1").addClass("selected");

    }


    tinymce.init({
        mode : "textareas",

        theme_advanced_buttons1 : "link, unlink,",
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : false,
        plugins: "paste,link,code",
        cleanup: false,
        verify_html: false

  
});


</script>
<%
    cerrarBaseDatos()
%>
