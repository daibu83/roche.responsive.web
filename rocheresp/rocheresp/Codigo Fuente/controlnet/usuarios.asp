﻿<%@ Language=JScript codepage="65001" %>
<!--#include file ="includes/seguridad.asp"-->
<!--#include file ="includes/variables.asp"-->
<!--#include file ="inicio.asp"-->
<!--#include file ="includes/funciones.asp"-->
<!--#include file ="includes/funcionesbasedatos.asp"-->
<!--#include file ="includes/buscadores.asp"-->
<%
abrirBaseDatos()
var IdUsuario=Request2("IdUsuario")

var strTextoBusqueda=""

if(Usuario !='')
    strTextoBusqueda+="Usuario: "+Usuario+". "
if(NombreApellidos !='')
    strTextoBusqueda+="Nombre y apellidos: "+NombreApellidos+". "


if(IdRol !='')
    strTextoBusqueda+="Rol: "+GetRol(IdRol)+". "
bOperar = false;
bOperar = GetOperar();   
%>
		<div id="bk">
		<!--#include file ="header.asp"-->
		<!--#include file ="tooltip.asp"-->
		<div id="container" class="clearfix">
			  <!--#include file ="menu.asp"-->
			<div id="page">
				<div class="menu clearfix">
					 <!-- PAGE TITLE --> 
					<div>Usuarios y Roles</div>
					<div class="cr_pass"></div>
				</div><!-- menu -->
				
				 <!-- PAGE CONTENT --> 
				<div class="clearfix content">
					 <!-- Tab bar component --> 
					<div class="clearfix tabbar usuarios">
						<ul class="tabs">
							<li class="tab1" id="tab1">
							<%
							
							var Ocultartab3=' style="display:none" '
							var Ocultartab4=' style="display:none" '
							var Seleccionartab1=' class="selected" '
							var Seleccionartab4=' class="selected" '
							if(IdUsuario!='')
							{
							    Ocultartab4=""
							    Seleccionartab1=""
							}
							else
							{
							    Ocultartab3=""
							    Seleccionartab4=""
							}
							
							%>
								<a id="a1" <%=Seleccionartab1%> onclick="showOnly('tab1','usuarios')" >
									Listado
								</a>
							</li>
							<li class="tab2" id="tab2">
								<a id="a2" onclick="showOnly('tab2','usuarios')">
									<img src="img/icons/magnifier.png" alt=""/>
									Buscador
								</a>
							</li>
							<li class="tab3" <%=Ocultartab3%> id="tab3">
								<a id="a3" onclick="showOnly('tab3','usuarios')">
									<img src="img/icons/plus-small.png" alt=""/>
									Usuario Nuevo
								</a>
							</li>
							<li class="tab4" <%=Ocultartab4%> id="tab4">
								<a id="a4" <%=Seleccionartab4%> onclick="showOnly('tab4','usuarios')">
									<img src="img/icons/edit.png" alt=""/>
									Ver editar
								</a>
							</li>
						</ul>
						<div class="tabContent clearfix">
						<!-- tab 1 content -->
							<div class="tab1">
								<%
								if(strTextoBusqueda!='')
								{
								%>
									<div class="box-padd">
									<p class="text filtrored">Atención, tienes filtros de búsqueda aplicados. Resultados de la búsqueda. <%=strTextoBusqueda%><strong></strong></p>
								</div><!-- box-padd -->
								<%
						        }
						        %>
								<table cellpadding="0" cellspacing="0" border="0" id="table"  class="uiTable tablesorter">  
								
									<thead> 
										<tr> 
											<th><h3>Nombre</h3></th> 
											<th><h3>Usuario</h3></th> 
											
											<th><h3>Rol</h3></th> 
											<th><h3>&Uacute;ltimo acceso</h3></th> 
											<th><h3>Editar</h3></th>
											<!--
											<th><h3>Eliminar</h3></th> 											
											-->
										</tr> 
									</thead> 
									<tbody> 
									
										<%
									var Msql="select dbo.Usuarios.IdUsuario, dbo.Usuarios.Usuario, dbo.Usuarios.Contrasena, dbo.Usuarios.Nombre, dbo.Usuarios.Apellidos, dbo.Usuarios.Tipo  from usuarios where  1=1 "
									if(IdRol!='')
									    Msql+=" and Usuarios.Tipo = '"+IdRol+"' "
									if(Usuario!='')
									    Msql+=" and Usuarios.usuario like '%"+Usuario+"%' "
									if(NombreApellidos!='')
									    Msql+=" and (Usuarios.nombre like '%"+NombreApellidos+"%' or  Usuarios.apellidos like '%"+NombreApellidos+"%' )"
									
									Msql+=" order by nombre,apellidos"
                                    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    								MiRecordset.Open(Msql,Conexion,3,1);
                                    var iNumUsuario=0
                                     if(MiRecordset.EOF)
                                    {
                                        Response.Write('<tr><td colspan="6">No hay datos para el criterio de busqueda</td></tr>')
                                    }
                                    while(!MiRecordset.EOF)
                                    {
                                        strNombre=""+MiRecordset("Nombre")
                                        strIdUsuario=""+MiRecordset("IdUsuario")
                                        strApellidos=""+MiRecordset("Apellidos")
                                        strUsuario=""+MiRecordset("Usuario")
                                        strTipo=""+MiRecordset("Tipo")
                                        if(strTipo=='G')
                                            strTipo="Administrador de contenidos"
                                        else if(strTipo=='A')
                                            strTipo="Administrador"
                                            
                                        strFechaLogueo=GetUltimoLogueo(strIdUsuario)
                                        strFechaLogueo=strFechaLogueo.substring(0,16)
                                        
									%>
									
										<tr> 
											<td style="width:200px"><%=strNombre%>&nbsp;<%=strApellidos%></td> 
											<td><%=strUsuario%></td> 
											
											<td><%=strTipo%></td> 
											<td><span class="center"><span class="center"><%=strFechaLogueo%></span></span></td>
											<td><a href="javascript:EditarUsuario('<%=strIdUsuario%>')" title="Editar"><img src="img/icons/edit.png" alt="Editar"/></a></td>  
											
										</tr> 
									<%
									    MiRecordset.MoveNext()
									    iNumUsuario++
									}
									MiRecordset.Close()
									%>
																		
									</tbody> 
								</table>
								 <!-- Navigation controls --> 
								<br class="clear" />
								<div class="barbottom">
									<!-- pagination -->
									<div id="pager" class="pager">
										<form action="#">
										<fieldset>
											<div class="fright">
												<a class="butNav first" href="#">&laquo; First</a>
												<a class="butNav prev" href="#">&laquo; Previous</a>
												<a class="butPage" href="#"><input type="text" class="pagedisplay"/></a>
												<a class="butNav next" href="#">Next &raquo;</a>
												<a class="butNav last" href="#">Last &raquo;</a>
											</div>
											<label for="sizefiltro" >Mostrar datos:</label>
											<select id="sizefiltro" class="pagesize" >
											    
												
												<%
												if(iNumUsuario>=10)
												    Response.Write('<option value="10">10</option>')
												if(iNumUsuario>=20)
												    Response.Write('<option value="20">20</option>')
												if(iNumUsuario>=30)
												    Response.Write('<option value="30">30</option>')    
												if(iNumUsuario>=40)
												    Response.Write('<option value="40">40</option>')    
												if(iNumUsuario!=10 && iNumUsuario!=20 && iNumUsuario!=30 && iNumUsuario!=40)    
												{
												%>
    												<option  value="<%=iNumUsuario%>"><%=iNumUsuario%></option>
    										    <%
    										    }
    										    %>
											</select>
											</fieldset>
										</form>
									</div><!-- pager -->
								</div><!-- barbottom -->
							</div><!-- tab1 -->
							
							<div class="tab2" style="display:none;">
    							<form name="Form1" id="Form1" method=post>
                                <input type=hidden name="IdUsuario" id="IdUsuario">    
                                <input type=hidden name="TipoBusqueda" id="TipoBusqueda" value="S">    
								<!--  Tab 2 content -->
								<div class="fields clearfix">
									<h2>Rellena los datos del formulario para buscar</h2>
									<p class="sep">
										<label class="medium" for="nombre">Usuario</label>
										<input type="text" value="<%=Usuario%>" class="sText" id="Usuario" name="Usuario"/>
									</p>
									<p class="sep">
										<label class="medium" for="nombre">Nombre y apellidos</label>
										<input type="text" value="<%=NombreApellidos%>" class="sText" id="NombreApellidos" name="NombreApellidos"/>
									</p>
									<p class="sep">
										<label class="medium" for="rol">Rol</label>
										<select class="sSelect" id="IdRol" name="IdRol">
											<% 
												DibujarRol(IdRol)
												%>
										</select>
									</p>
									
								</div><!-- fields -->
								<div class="boxBut"><input type="submit" value="Buscar" class="butDef" /></div><!-- boxBut -->
								<div class="boxButfiltro"><input  id="clearfilter" value="Eliminar Filtros" class="butDef" /></div>
	                            </form>							
							</div><!-- tab2 -->
							
							<div class="tab3" style="display:none;">
								<!--  Tab 3 content -->
									<div class="fields clearfix">
									<form name="Form3" id="Form3" method=post target="frEjecutar" action="usuariosalta.asp">
										<h2>Rellena los datos del formulario para dar de alta a un nuevo usuario
										<a href="javascript:CerrarNuevo()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										
										<p class="sep">
											<label class="medium" for="rolAdd1">Rol</label>
											<select class="sSelect" id="strUnicoTipoN" name="strUnicoTipoN">
												<%
												DibujarRol(strUnicoTipo)
												%>
											</select>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Nombre</label>
											<input type="text" value="<%=strUnicoNombre%>" class="sText" id="strUnicoNombreN" name="strUnicoNombreN"/>
										</p>
										<p class="sep">
											<label class="medium" for="apellido1Add2">Apellidos</label>
											<input type="text" value="<%=strUnicoApellidos%>" class="sText" id="strUnicoApellidosN" name="strUnicoApellidosN"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreUsuAdd1">Nombre Usuario</label>
											<input type="text" value="<%=strUnicoUsuario%>" class="sText" id="strUnicoUsuarioN" name="strUnicoUsuarioN"/>
										</p>
										<p class="sep">
											<label class="medium" for="claveAdd1">Clave de Acceso</label>
											<input type="text" value="<%=strUnicoContrasena%>" class="sText" id="strUnicoContrasenaN" name="strUnicoContrasenaN"/>
										</p>
										<p class="sep">
											<label class="medium" for="mailAdd1">Mail</label>
											<input type="text" value="<%=strUnicoEmail%>" class="sText" id="strUnicoEmailN" name="strUnicoEmailN"/>
										</p>
										<p class="sep">
											<label class="medium" for="tfnoAdd1">Tel&eacute;fono</label>
											<input type="text" value="<%=strUnicoTelefono%>" class="sText" id="strUnicoTelefonoN" name="strUnicoTelefonoN"/>
										</p>
										
										
										
									</div><!-- fields -->
									<div class="fields">
										<label class="fleft medium" for="descripcionAdd1">Descripci&oacute;n</label>
										<div class="fleft"><textarea cols="50" rows="10" id="strUnicoDescripcionN" name="strUnicoDescripcionN" ><%=strUnicoDescripcion%></textarea></div>
									</div><!-- fields -->
									<div class="fields">
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activado</label>
											<select class="sSelect" id="strUnicoActivadoN" name="strUnicoActivadoN">
												<%
												DibujarActivados('')
												%>
											</select>
										</p>
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarNuevo()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Enviar" class="butDef" onclick="javascript:EnviarAlta()"/>
									</div><!-- fright -->
									</form>
							</div><!-- tab3-->
							<div class="tab4" style="display:none;">
								
								<form name="Form2" id="Form2" method=post target="frEjecutar" action="usuariosguardar.asp">
									<div class="fields clearfix">
									<% 
										
										
										var strUnicoIdUsuario=""
                                        var strUnicoTipo=""
                                        var strUnicoNombre=""
                                        var strUnicoApellidos=""
                                        var strUnicoUsuario=""
                                        var strUnicoContrasena=""
                                        var strUnicoEmail=""
                                        var strUnicoTelefono=""
                                        var strUnicoDescripcion=""
                                        var strUnicoActivado=""
                                        
                                        if(IdUsuario!='')
                                        {
                                            var strSelect=" select * from usuarios where idusuario='"+IdUsuario+"' "
                                            
                                            MiRecordset.Open(strSelect,Conexion,3,1);
                                            
                                            if(!MiRecordset.EOF)
                                            {
                                                strUnicoIdUsuario=""+MiRecordset("IdUsuario")
                                                strUnicoTipo=""+MiRecordset("Tipo")
                                                strUnicoNombre=""+MiRecordset("Nombre")
                                                strUnicoApellidos=""+MiRecordset("Apellidos")
                                                strUnicoUsuario=""+MiRecordset("Usuario")
                                                strUnicoContrasena=""+MiRecordset("Contrasena")
                                                strUnicoEmail=""+MiRecordset("Email")
                                                strUnicoTelefono=""+MiRecordset("Telefono")
                                                strUnicoDescripcion=""+MiRecordset("Descripcion")
                                                strUnicoActivado=""+MiRecordset("Activo")
                                                
								            }
								            MiRecordset.Close()
								      	
								        }
								        
								        //Response.Write("//"+strUnicoTipo)
										%>
										<input type=hidden name="strUnicoIdUsuario" id="strUnicoIdUsuario" value="<%=strUnicoIdUsuario%>"/>
										<h2>Modifica los datos del formulario para editar el usuario
										<a href="javascript:CerrarEditar()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										
										<p class="sep">
											<label class="medium" for="rolAdd1">Rol</label>
											<select class="sSelect" id="strUnicoTipo" name="strUnicoTipo">
												<%
												DibujarRol(strUnicoTipo)
												%>
											</select>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Nombre</label>
											<input type="text" value="<%=strUnicoNombre%>" class="sText" id="strUnicoNombre" name="strUnicoNombre"/>
										</p>
										<p class="sep">
											<label class="medium" for="apellido1Add2">Apellidos</label>
											<input type="text" value="<%=strUnicoApellidos%>" class="sText" id="strUnicoApellidos" name="strUnicoApellidos"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreUsuAdd1">Nombre Usuario</label>
											<input type="text" value="<%=strUnicoUsuario%>" class="sText" id="strUnicoUsuario" name="strUnicoUsuario"/>
										</p>
										<p class="sep">
											<label class="medium" for="claveAdd1">Clave de Acceso</label>
											<input type="text" value="<%=strUnicoContrasena%>" class="sText" id="strUnicoContrasena" name="strUnicoContrasena"/>
										</p>
										<p class="sep">
											<label class="medium" for="mailAdd1">Mail</label>
											<input type="text" value="<%=strUnicoEmail%>" class="sText" id="strUnicoEmail" name="strUnicoEmail"/>
										</p>
										<p class="sep">
											<label class="medium" for="tfnoAdd1">Tel&eacute;fono</label>
											<input type="text" value="<%=strUnicoTelefono%>" class="sText" id="strUnicoTelefono" name="strUnicoTelefono"/>
										</p>
										
										
										
									</div><!-- fields -->
									<div class="fields">
										<label class="fleft medium" for="descripcionAdd1">Descripci&oacute;n</label>
										<div class="fleft"><textarea cols="50" rows="10" id="strUnicoDescripcion" name="strUnicoDescripcion" ><%=strUnicoDescripcion%></textarea></div>
									</div><!-- fields -->
									<div class="fields">
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activado</label>
											<select class="sSelect" id="strUnicoActivado" name="strUnicoActivado">
												<%
												DibujarActivados(strUnicoActivado)
												%>
											</select>
										</p>
									</div><!-- fields -->
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarEditar()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Guardar" class="butDef" onclick="javascript:EnviarEditar()"/>
									</div><!-- fright-->
									</form>
							</div><!-- tab4-->
						</div><!-- tabContent-->
					</div><!-- tabbar-->
					<!--  PAGE CONTENT  -->
				</div><!-- content -->
			</div><!-- page -->
		</div><!-- container -->
		</div><!-- bk -->
		
		
		 
<iframe id="frEjecutar" name="frEjecutar" width="1" height="0" frameborder="0" ></iframe>

	</body>
</html>
<script type="text/javascript">

    $(function() {
        $('#clearfilter').click(function() {
            document.getElementById('frEjecutar').src = "includes/limpiarfiltros.asp"
        });
    });
IdUsuario='<%=IdUsuario%>'


if(IdUsuario!='')
showOnly('tab4','usuarios')

$(function(){
    $("#table") 
    .tablesorter({widthFixed: false, widgets: ['zebra']}) 
    .tablesorterPager({container: $("#pager")});   

});

function EditarUsuario(IdUsuario)
{
    document.getElementById('IdUsuario').value=IdUsuario
    document.getElementById('Form1').submit()
}
function CerrarEditar()
{
    document.getElementById('tab4').style.display="none"
    document.getElementById('tab3').style.display="block"
    showOnly('tab1','usuarios')
    $(".tabbar .tabs a").removeClass('selected');
	$("#a1").addClass("selected");
     
}
function EnviarEditar()
{
    
    
    if (Trim(document.getElementById('strUnicoTipo').value) == null || Trim(document.getElementById('strUnicoTipo').value) == "")
    {
        alert ("Campo rol obligatorio");
        document.getElementById('strUnicoTipo').focus()
        return;
    }
    
    if (Trim(document.getElementById('strUnicoNombre').value) == null || Trim(document.getElementById('strUnicoNombre').value) == "")
    {
        alert ("Campo nombre obligatorio");
        document.getElementById('strUnicoNombre').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoUsuario').value) == null || Trim(document.getElementById('strUnicoUsuario').value) == "")
    {
        alert ("Campo usuario obligatorio");
        document.getElementById('strUnicoUsuario').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoContrasena').value) == null || Trim(document.getElementById('strUnicoContrasena').value) == "")
    {
        alert ("Campo clave de acceso obligatorio");
        document.getElementById('strUnicoContrasena').focus()
        return;
    }
    
    if (Trim(document.getElementById('strUnicoActivado').value) == null || Trim(document.getElementById('strUnicoActivado').value) == "")
    {
        alert ("Campo activado obligatorio");
        document.getElementById('strUnicoActivado').focus()
        return;
    }
    
    if (!Confirmar("¿Seguro que quiere modificar estos datos?"))
    return

    document.getElementById('Form2').submit()
}
function EnviarAlta()
{
	  
    if (Trim(document.getElementById('strUnicoTipoN').value) == null || Trim(document.getElementById('strUnicoTipoN').value) == "")
    {
        alert ("Campo rol obligatorio");
        document.getElementById('strUnicoTipo').focus()
        return;
    }
    
    if (Trim(document.getElementById('strUnicoNombreN').value) == null || Trim(document.getElementById('strUnicoNombreN').value) == "")
    {
        alert ("Campo nombre obligatorio");
        document.getElementById('strUnicoNombre').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoUsuarioN').value) == null || Trim(document.getElementById('strUnicoUsuarioN').value) == "")
    {
        alert ("Campo usuario obligatorio");
        document.getElementById('strUnicoUsuario').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoContrasenaN').value) == null || Trim(document.getElementById('strUnicoContrasenaN').value) == "")
    {
        alert ("Campo clave de acceso obligatorio");
        document.getElementById('strUnicoContrasena').focus()
        return;
    }
    
    if (Trim(document.getElementById('strUnicoActivadoN').value) == null || Trim(document.getElementById('strUnicoActivadoN').value) == "")
    {
        alert ("Campo activado obligatorio");
        document.getElementById('strUnicoActivado').focus()
        return;
    }
    

    if (!Confirmar("¿Seguro que quiere dar de alta un usuario con estos datos?"))
        return

    document.getElementById('Form3').submit()
}
function CerrarNuevo()
{
    showOnly('tab1','usuarios')
    
   	$(".tabbar .tabs a").removeClass('selected');
    $("#a1").addClass("selected");
 
}
</script>
<%
cerrarBaseDatos()
%>
