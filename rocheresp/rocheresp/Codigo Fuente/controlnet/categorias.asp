﻿<%@ Language=JScript codepage="65001" %>
<!--#include file ="includes/seguridad.asp"-->
<!--#include file ="includes/variables.asp"-->
<!--#include file ="inicio.asp"-->
<!--#include file ="includes/funciones.asp"-->
<!--#include file ="includes/funcionesbasedatos.asp"-->
<!--#include file ="includes/buscadores.asp"-->
<%
abrirBaseDatos()
var IdCategoria = Request2("IdCategoria")


var strTextoBusqueda=""

if(Categoria !='')
    strTextoBusqueda += "Categoría : " + Categoria + ". "


bOperar = false;
bOperar = GetOperar();
    
%>
		<div id="bk">
		<!--#include file ="header.asp"-->
		<!--#include file ="tooltip.asp"-->
		<div id="container" class="clearfix">
			  <!--#include file ="menu.asp"-->
			<div id="page">
				<div class="menu clearfix">
					 <!-- PAGE TITLE --> 
					<div>Categorías</div>
					<div class="cr_pass"></div>
				</div><!-- menu -->
				
				 <!-- PAGE CONTENT --> 
				<div class="clearfix content">
					 <!-- Tab bar component --> 
					<div class="clearfix tabbar Categorias">
						<ul class="tabs">
							<li class="tab1" id="tab1">
							<%
							
							var Ocultartab3=' style="display:none" '
							var Ocultartab4=' style="display:none" '
							var Seleccionartab1=' class="selected" '
							var Seleccionartab4=' class="selected" '
							if (IdCategoria != '')
							{
							    Ocultartab4=""
							    Seleccionartab1=""
							}
							else
							{
							    Ocultartab3=""
							    Seleccionartab4=""
							}
							
							%>
								<a id="a1" <%=Seleccionartab1%> onclick="showOnly('tab1','Categorias')" >
									Listado
								</a>
							</li>
							<li class="tab2" id="tab2">
								<a id="a2" onclick="showOnly('tab2','Categorias')">
									<img src="img/icons/magnifier.png" alt=""/>
									Buscador
								</a>
							</li>
							<li class="tab3" <%=Ocultartab3%> id="tab3">
								<a id="a3" onclick="showOnly('tab3','Categorias')">
									<img src="img/icons/plus-small.png" alt=""/>
									Categoría Nueva
								</a>
							</li>
							<li class="tab4" <%=Ocultartab4%> id="tab4">
								<a id="a4" <%=Seleccionartab4%> onclick="showOnly('tab4','Categorias')">
									<img src="img/icons/edit.png" alt=""/>
									Ver editar
								</a>
							</li>
						</ul>
						<div class="tabContent clearfix">
						<!-- tab 1 content -->
							<div class="tab1">
								<%
								if(strTextoBusqueda!='')
								{
								%>
									<div class="box-padd">
									<p class="text filtrored">Atención, tienes filtros de búsqueda aplicados. Resultados de la búsqueda. <%=strTextoBusqueda%><strong></strong></p>
								</div><!-- box-padd -->
								<%
						        }
						        %>
								<table cellpadding="0" cellspacing="0" border="0" id="table"  class="uiTable tablesorter">  
								
									<thead> 
										<tr>
											
											<th><h3>Categoria</h3></th> 
											<th><h3>Columna</h3></th>
											<th><h3>Orden</h3></th> 
											<th><h3>Activo</h3></th> 
											<th><h3>Editar</h3></th>
											<!--
											<th><h3>Eliminar</h3></th> 											
											-->
										</tr> 
									</thead> 
									<tbody> 
									
										<%
										
									var Msql="select actividades_categorias.*  from actividades_categorias where IdCategoria>0  "
									if(Categoria!='')
									    Msql+=" and Categoria like '%"+Categoria+"%' "
									Msql+=" order by columna,orden,categoria "
                                    var MiRecordset =  Server.CreateObject("ADODB.Recordset");
    								MiRecordset.Open(Msql,Conexion,3,1);
                                    var iNumCategorias=0
                                     if(MiRecordset.EOF)
                                    {
                                        Response.Write('<tr><td colspan="4">No hay datos para el criterio de busqueda</td></tr>')
                                    }
                                    while(!MiRecordset.EOF)
                                    {
                                        strCategoria=""+MiRecordset("Categoria")
                                        strIdCategoria=""+MiRecordset("IdCategoria")
                                        strActiva=""+MiRecordset("Activa")
                                        strColumna=""+MiRecordset("Columna")
                                        strOrden=""+MiRecordset("Orden")
                                        strIcono="minus-circle.png"
                                        if(strActiva==1)
                                            strIcono="tick.png"
                                        
                                        
                                            
                                        
                                        
									%>
									
										<tr> 
											<td style="width:200px"><%=strCategoria%></td> 
											<td style="width:200px"><%=strColumna%></td> 
											<td style="width:200px"><%=strOrden%></td> 
											<td><img src="img/icons/<%=strIcono%>" class="icon" alt="Categorias"/></td> 

											<td><a href="javascript:EditarCategoria('<%=strIdCategoria%>')" title="Editar"><img src="img/icons/edit.png" alt="Editar"/></a></td>  
											
										</tr> 
									<%
									    MiRecordset.MoveNext()
									    iNumCategorias++
									}
									MiRecordset.Close()
									%>
																		
									</tbody> 
								</table>
								 <!-- Navigation controls --> 
								<br class="clear" />
								<div class="barbottom">
									<!-- pagination -->
									<div id="pager" class="pager">
										<form action="#">
										<fieldset>
											<div class="fright">
												<a class="butNav first" href="#">&laquo; First</a>
												<a class="butNav prev" href="#">&laquo; Previous</a>
												<a class="butPage" href="#"><input type="text" class="pagedisplay"/></a>
												<a class="butNav next" href="#">Next &raquo;</a>
												<a class="butNav last" href="#">Last &raquo;</a>
											</div>
											<label for="sizefiltro" >Mostrar datos:</label>
											<select id="sizefiltro" class="pagesize" >
											    
												
												<%
												if(iNumCategorias>=10)
												    Response.Write('<option value="10">10</option>')
												if(iNumCategorias>=20)
												    Response.Write('<option value="20">20</option>')
												if(iNumCategorias>=30)
												    Response.Write('<option value="30">30</option>')    
												if(iNumCategorias>=40)
												    Response.Write('<option value="40">40</option>')    
												if(iNumCategorias!=10 && iNumCategorias!=20 && iNumCategorias!=30 && iNumCategorias!=40)    
												{
												%>
    												<option  value="<%=iNumCategorias%>"><%=iNumCategorias%></option>
    										    <%
    										    }
    										    %>
											</select>
											</fieldset>
										</form>
									</div><!-- pager -->
								</div><!-- barbottom -->
							</div><!-- tab1 -->
							
							<div class="tab2" style="display:none;">
    							<form name="Form1" id="Form1" method=post>
                                <input type=hidden name="IdCategoria" id="IdCategoria">    
                                <input type=hidden name="TipoBusqueda" id="TipoBusqueda" value="S">    
								<!--  Tab 2 content -->
								<div class="fields clearfix">
									<h2>Rellena los datos del formulario para buscar</h2>
									<p class="sep">
										<label class="medium" for="nombre">Categoria</label>
										<input type="text" value="<%=Categoria%>" class="sText" id="Categoria" name="Categoria"/>
									</p>
									
								
								</div><!-- fields -->
								<div class="boxBut"><input type="submit" value="Buscar" class="butDef" /></div><!-- boxBut -->
								<div class="boxButfiltro"><input  id="clearfilter" value="Eliminar Filtros" class="butDef" /></div>
	                            </form>							
							</div><!-- tab2 -->
							
							<div class="tab3" style="display:none;">
								<!--  Tab 3 content -->
									<div class="fields clearfix">
									<form name="Form3" id="Form3" method=post target="frEjecutar" action="Categoriasalta.asp" >
									    <input type=hidden name="strUnicoImagen1IDN" id="strUnicoImagen1IDN" value=""/>
										<h2>Rellena los datos del formulario para dar de alta una nueva Categoría
										<a href="javascript:CerrarNuevo()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Categoría</label>
											<input type="text" value="" class="sText large" id="strUnicoCategoriaN" name="strUnicoCategoriaN"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Columna</label>
											<select class="sSelect" id="strUnicoColumnaN" name="strUnicoColumnaN">
												<%
												    DibujarColumnas('')
												%>
											</select>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrdenN" name="strUnicoOrdenN">
												<%
												    DibujarOrden('')
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activo</label>
											<select class="sSelect" id="strUnicoActivoN" name="strUnicoActivoN">
												<%
												DibujarActivadosBinario('')
												%>
											</select>
										</p>
										
										
										
									
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarNuevo()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Enviar" class="butDef" onclick="javascript:EnviarAlta()"/>
									</div><!-- fright -->
									</form>
							</div><!-- tab3-->
							<div class="tab4" style="display:none;">
								
								<form name="Form2" id="Form2" method=post target="frEjecutar" action="Categoriasguardar.asp"  >
								    <input type=hidden name="strUnicoImagen1ID" id="strUnicoImagen1ID" value=""/>
									<div class="fields clearfix">
									<% 
										
										
										var strUnicoIdCategoria=""
                                        var strUnicoCategoria=""
                                        var strUnicoActiva=""
                                        var strUnicoOrden = 1;
                                        var strUnicoColumna = 1;
                                        if(IdCategoria!='')
                                        {
                                            var strSelect = " select * from actividades_categorias where IdCategoria='" + IdCategoria + "' "
                                            
                                            MiRecordset.Open(strSelect,Conexion,3,1);
                                            
                                            if(!MiRecordset.EOF) {
                                                strUnicoIdCategoria = "" + MiRecordset("idcategoria")
                                                strUnicoCategoria = "" + MiRecordset("categoria")
                                                strUnicoColumna = "" + MiRecordset("columna")
                                                strUnicoOrden = "" + MiRecordset("orden")
                                                strUnicoActiva=""+MiRecordset("Activa")
                                                
								            }
								            MiRecordset.Close()
								      	
								        }
								        
								        //Response.Write("//"+strUnicoTipo)
										%>
										<input type=hidden name="strUnicoIdCategoria" id="strUnicoIdCategoria" value="<%=strUnicoIdCategoria%>"/>
										<h2>Modifica los datos del formulario para editar la Categoría
										<a href="javascript:CerrarEditar()" class="fright butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a></h2>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Categoría</label>
											<input type="text" value="<%=strUnicoCategoria%>" class="sText large" id="strUnicoCategoria" name="strUnicoCategoria"/>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Columna</label>
											<select class="sSelect" id="strUnicoColumna" name="strUnicoColumna">
												<%
												    DibujarColumnas(strUnicoColumna)
												%>
											</select>
										</p>
										<p class="sep">
											<label class="medium" for="nombreAdd1">Orden</label>
											<select class="sSelect" id="strUnicoOrden" name="strUnicoOrden">
												<%
												    DibujarOrden(strUnicoOrden)
												%>
											</select>
										</p>
										
										<p class="sep">
											<label class="medium" for="activadaAdd1">Activa</label>
											<select class="sSelect" id="strUnicoActivo" name="strUnicoActivo">
												<%
												DibujarActivadosBinario(strUnicoActiva)
												%>
											</select>
										</p>
										
										
									</div>
									<div class="lineBottom"></div>
									<div class="fright"> 
										<a href="javascript:CerrarEditar()" class="butSim"><img src="img/icons/cross.png" alt="" class="imgIco" />Cerrar</a>
										<input type=button value="Guardar" class="butDef" onclick="javascript:EnviarEditar()"/>
									</div><!-- fright-->
									</form>
							</div><!-- tab4-->
						</div><!-- tabContent-->
					</div><!-- tabbar-->
					<!--  PAGE CONTENT  -->
				</div><!-- content -->
			</div><!-- page -->
		</div><!-- container -->
		</div><!-- bk -->
		
		
		 
<iframe id="frEjecutar" name="frEjecutar" width="1" height="1" frameborder="0" ></iframe>

	</body>
</html>
<script type="text/javascript">

$(function() {
    $('#clearfilter').click(function() {
        document.getElementById('frEjecutar').src = "includes/limpiarfiltros.asp"
    });
});

IdCategoria = '<%=IdCategoria%>'


if (IdCategoria != '')
    showOnly('tab4','Categorias')

$(function(){
    $("#table") 
    .tablesorter({widthFixed: false, widgets: ['zebra']}) 
    .tablesorterPager({container: $("#pager")});   

});

function EditarCategoria(IdCategoria)
{
    document.getElementById('IdCategoria').value = IdCategoria
    document.getElementById('Form1').submit()
}
function CerrarEditar()
{
    document.getElementById('tab4').style.display="none"
    document.getElementById('tab3').style.display="block"
    showOnly('tab1','Categorias')
    $(".tabbar .tabs a").removeClass('selected');
	$("#a1").addClass("selected");
     
}
function EnviarEditar()
{

    if (Trim(document.getElementById('strUnicoCategoria').value) == null || Trim(document.getElementById('strUnicoCategoria').value) == "") {
        alert("Campo Categoría obligatorio");
        document.getElementById('strUnicoCategoria').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoColumna').value) == null || Trim(document.getElementById('strUnicoColumna').value) == "") {
        alert("Campo Columna obligatorio");
        document.getElementById('strUnicoOrden').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoOrden').value) == null || Trim(document.getElementById('strUnicoOrden').value) == "") {
        alert("Campo Orden obligatorio");
        document.getElementById('strUnicoColumna').focus()
        return;
    }
        
    
     if (Trim(document.getElementById('strUnicoActivo').value) == null || Trim(document.getElementById('strUnicoActivo').value) == "")
    {
        alert("Campo Activo obligatorio");
        document.getElementById('strUnicoActivo').focus()
        return;
    }
    
    
    if (!Confirmar("¿Seguro que quiere modificar estos datos?"))
    return

    document.getElementById('Form2').submit()
}
function EnviarAlta()
{


    if (Trim(document.getElementById('strUnicoCategoriaN').value) == null || Trim(document.getElementById('strUnicoCategoriaN').value) == "") {
        alert("Campo Categoría obligatorio");
        document.getElementById('strUnicoCategoriaN').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoColumnaN').value) == null || Trim(document.getElementById('strUnicoColumnaN').value) == "") {
        alert("Campo Columna obligatorio");
        document.getElementById('strUnicoColumnaN').focus()
        return;
    }
    if (Trim(document.getElementById('strUnicoOrdenN').value) == null || Trim(document.getElementById('strUnicoOrdenN').value) == "") {
        alert("Campo Orden obligatorio");
        document.getElementById('strUnicoOrdenN').focus()
        return;
    }


    if (Trim(document.getElementById('strUnicoActivoN').value) == null || Trim(document.getElementById('strUnicoActivoN').value) == "") {
        alert("Campo Activo obligatorioN");
        document.getElementById('strUnicoActivoN').focus()
        return;
    }
     
   
    

    if (!Confirmar("¿Seguro que quiere dar de alta una Categoría con estos datos?"))
        return

    document.getElementById('Form3').submit()
}
function CerrarNuevo()
{
    showOnly('tab1','Categorias')
    
   	$(".tabbar .tabs a").removeClass('selected');
    $("#a1").addClass("selected");

}
tinymce.init({
    mode: "textareas",

    theme_advanced_buttons1: "link, unlink,",
    force_br_newlines: true,
    force_p_newlines: false,
    forced_root_block: false,
    plugins: "paste,link,code",
    cleanup: false,
    verify_html: false


});
</script>
<%
cerrarBaseDatos()
%>
