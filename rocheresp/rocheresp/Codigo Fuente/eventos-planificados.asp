<%@ Language=JScript codepage="65001" %>
<!--#include file ="controlnet/includes/variables.asp"-->
<!--#include file ="controlnet/includes/funciones.asp"-->
<!--#include file ="controlnet/includes/funcionesbasedatos.asp"-->
<!DOCTYPE html>
<%
menuactivo = 'planificaciones'
    abrirBaseDatos()
    select = "select *  from planificaciones where activo='1'order by orden  "
    var MiRecordset = Server.CreateObject("ADODB.Recordset");
    MiRecordset.Open(select, Conexion, 3, 1);
    aplanificaciones = new Array();
    
    while (!MiRecordset.EOF) {

        aplanificaciones[String(MiRecordset('idplanificacion'))] = new Array();
        aplanificaciones[String(MiRecordset('idplanificacion'))]['planificacion'] = String(MiRecordset('planificacion'));
        aplanificaciones[String(MiRecordset('idplanificacion'))]['pasos'] = new Array();
        select = "select *  from planificacionespasos where activo=1 and idplanificacion='" + String(MiRecordset('idplanificacion')) + "' order by orden "
        var MiRecordset1 = Server.CreateObject("ADODB.Recordset");
        MiRecordset1.Open(select, Conexion, 3, 1);
        while (!MiRecordset1.EOF) {

            aplanificaciones[String(MiRecordset('idplanificacion'))]['pasos'][String(MiRecordset1('idpaso'))] = new Array();
            aplanificaciones[String(MiRecordset('idplanificacion'))]['pasos'][String(MiRecordset1('idpaso'))]['paso'] = String(MiRecordset1('paso'));
            aplanificaciones[String(MiRecordset('idplanificacion'))]['pasos'][String(MiRecordset1('idpaso'))]['que'] = String(MiRecordset1('que'));
            aplanificaciones[String(MiRecordset('idplanificacion'))]['pasos'][String(MiRecordset1('idpaso'))]['quien'] = String(MiRecordset1('quien'));
            aplanificaciones[String(MiRecordset('idplanificacion'))]['pasos'][String(MiRecordset1('idpaso'))]['donde'] = String(MiRecordset1('donde'));
            aplanificaciones[String(MiRecordset('idplanificacion'))]['pasos'][String(MiRecordset1('idpaso'))]['cuando'] = String(MiRecordset1('cuando'));
            
            MiRecordset1.MoveNext();
        }
        MiRecordset1.Close();
        MiRecordset.MoveNext();
    }

    MiRecordset.Close();
                                    
%><html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Roche</title>		
		<meta name="description" content="">
		<meta name="author" content="f2f digital">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">


		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

	</head>
	<body>
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="index.asp">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="img/logo.png">
						</a>
					</div>
					
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						
						<!--#include file ="navegacion.asp"-->
					</div>
				</div>
			</header>

			<div role="main" class="main">

				<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li><a href="index.asp">Inicio</a></li>
									<li class="active">Planificación</li>
								</ul>
							</div>
						</div>

					</div>
				</section>
                
                <%
                var ipos=0;
                for(key in aplanificaciones )
                {%>
                <div class="container">
                    <div class="row">
						<div class="col-md-12">
                <%
                    if(ipos==0)
                        Response.Write('<h2 class="shorter"><strong>Planificación</strong></h2>')
                    else
                        Response.Write('<hr class="tall" />')
                %>
                    <h3><%=aplanificaciones[key]['planificacion']%></h3>
					<h3 class="short"><strong>Pasos</strong></h3>
					<p>Pulsa sobre cada uno de los pasos para ver más información.</p>
					    <div class="toggle" data-plugin-toggle data-plugin-options='{ "isAccordion": true }'>
					    <%
					    for(paso in aplanificaciones[key]['pasos'] )
					    {
					    %>
					        <section class="toggle"> <!--active-->
								<label><%=aplanificaciones[key]['pasos'][paso]['paso']%></label>
								<div class="toggle-content">
								<table>
									<tr>
										<th><strong>QUÉ</strong></th>
										<th><strong>QUIÉN</strong></th>
										<th><strong>DÓNDE</strong></th>
										<th><strong>CUÁNDO</strong></th>
									</tr>
									<tr>
										<td width="60%">
										<p><%=aplanificaciones[key]['pasos'][paso]['que']%></p>
										</td>
										<td width="20%">
										<p><%=aplanificaciones[key]['pasos'][paso]['quien']%></p>
										</td>
										<td width="15%">
										<p><%=aplanificaciones[key]['pasos'][paso]['donde']%></p>
										</td>
										<td>
										<p><%=aplanificaciones[key]['pasos'][paso]['cuando']%></p>
										</td>
									</tr>
								</table>
								</div>
							</section>
                        <%
                        }
                        %>					 
					 
					    </div>
						</div>

					</div>

				</div>
                <%
                    ipos++;
                }
                %>
                
			<footer id="footer">
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<p>© Copyright 2015. All Rights Reserved.</p>
							</div>
							
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.js"></script>
  

		<script src="vendor/jquery.easing/jquery.easing.js"></script>
		<script src="vendor/bootstrap/bootstrap.js"></script>
		<script src="vendor/common/common.js"></script>		

		<!-- Theme Base, Components and Settings 	-->
		<script src="js/theme.js"></script>
	
		<!-- Theme Custom         	-->
		<script src="js/custom.js"></script>

		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>


	</body>
</html>
