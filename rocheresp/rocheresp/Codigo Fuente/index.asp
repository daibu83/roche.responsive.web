<%@ Language=JScript codepage="65001" %>
<!--#include file ="controlnet/includes/variables.asp"-->
<!--#include file ="controlnet/includes/funciones.asp"-->
<!--#include file ="controlnet/includes/funcionesbasedatos.asp"-->
<!DOCTYPE html>
<%
menuactivo = 'definiciones'
abrirBaseDatos()
select="select *  from definiciones where activo='1' "
var MiRecordset =  Server.CreateObject("ADODB.Recordset");
MiRecordset.Open(select,Conexion,3,1);
adefiniciones = new Array();
atitular = new Array();
atitular[0] = new Array();
atitular[0]['titulo'] = '';
atitular[0]['definicion'] = '';

while(!MiRecordset.EOF)
{
    if (String(MiRecordset('tipo')) != 'titular') {
        adefiniciones[String(MiRecordset('iddefinicion'))] = new Array();
        adefiniciones[String(MiRecordset('iddefinicion'))]['titulo'] = String(MiRecordset('titulo'));
        adefiniciones[String(MiRecordset('iddefinicion'))]['definicion'] = String(MiRecordset('definicion'));
    }
    else {
        atitular[0]['titulo'] = String(MiRecordset('titulo'));
        atitular[0]['definicion'] = String(MiRecordset('definicion'));
    }
    MiRecordset.MoveNext();
}

MiRecordset.Close();
                                    
%>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Roche</title>		
		<meta name="description" content="">
		<meta name="author" content="f2f digital">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">


		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

	</head>
	<body>
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="index.asp">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="img/logo.png">
						</a>
					</div>
					
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						
						<!--#include file ="navegacion.asp"-->
					</div>
				</div>
			</header>


			<div role="main" class="main">

				<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li class="active">Inicio</li>
								</ul>
							</div>
						</div>
						
					</div>
				</section>

				<div class="container">

					<div class="row center">
						<div class="col-md-12">
                        
							<h2><strong><%=atitular[0]['titulo'] %></strong></h2>

							<p class="tall">
								<%=atitular[0]['definicion'] %>
							</p>
						</div>
					</div>

					<hr class="tall" />
				</div>

				<div class="container">

					<div class="row">
						<div class="col-md-12">
                        
							<div class="row definitions">
							<%
							
							for(key in adefiniciones)
							{
							%>
							    <div class="col-md-6">
									<div class="feature-box secundary">
										<div class="feature-box-icon">
											<i class="fa fa-tags"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter"><%=adefiniciones[key]['titulo']%></h4>
											<%=adefiniciones[key]['definicion']%>
										</div>
									</div>
								</div>
							<%
							}
							%>
							
								
								
							</div>
						</div>
					</div>

				</div>

				<!--section class="featured highlight footer">
					<div class="container">
						<div class="row center counters">
							<div class="col-md-3 col-sm-6">
								<strong data-to="7000" data-append="+">0</strong>
								<label>Happy Clients</label>
							</div>
							<div class="col-md-3 col-sm-6">
								<strong data-to="15">0</strong>
								<label>Years in Business</label>
							</div>
							<div class="col-md-3 col-sm-6">
								<strong data-to="352">0</strong>
								<label>Cups of Coffee</label>
							</div>
							<div class="col-md-3 col-sm-6">
								<strong data-to="159">0</strong>
								<label>High Score</label>
							</div>
						</div>
					</div>
				</section-->




				

			</div>

			<footer id="footer">
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<p>© Copyright 2015. All Rights Reserved.</p>
							</div>
							
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.js"></script>
  

		<script src="vendor/jquery.easing/jquery.easing.js"></script>
		<script src="vendor/bootstrap/bootstrap.js"></script>
		<script src="vendor/common/common.js"></script>		

		<!-- Theme Base, Components and Settings 	-->
		<script src="js/theme.js"></script>
	
		<!-- Theme Custom         	-->
		<script src="js/custom.js"></script>

		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>


	</body>
</html>
<%
cerrarBaseDatos()
%>